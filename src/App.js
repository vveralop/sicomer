import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import axios from 'axios';
import CryptoJS from 'crypto-js';
import base64url from 'base64url';
import Menusicomer from './menus/menusicomer';
import { data, calculo, urlLogin } from './configuraciones/data';
const jwt = require('jsonwebtoken');

const secret = base64url.encode(Buffer.from(data), 'utf8');

export class App extends Component {
  constructor(props) {
    super()

    this.state = {
      txtUsuario: "",
      txtNombre: "",
      txtPassword: "",
      accesoSistema: "",
      txtEmpresa: "",
      txtnombreempresa: "",
      logMenu: false
    }

    this.handleChange = this.handleChange.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.componentWillUnmount = this.componentWillUnmount.bind(this);
    this.limpiezaMenu = this.limpiezaMenu.bind(this);
    this.handleClick = this.handleClick.bind(this);
    this.entradaMenu = this.entradaMenu.bind(this);
  }

  componentWillUnmount() {
    this.setState({ logMenu: false })
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleClick = async (e) => {
    e.preventDefault();
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/login',
      'mode': 'no-cors'
    };
    const ttoClave = CryptoJS.HmacSHA256(this.state.txtPassword, secret).toString();
    const payload = jwt.sign(({
      idusuario: this.state.txtUsuario,
      claveusuario: ttoClave
    }), secret, { algorithm: calculo });

    await axios.post(urlLogin + '/login', { usuario: payload }, { headers })
      .then(response => {
        const resp = JSON.stringify(jwt.decode(response.data, secret, { algorithm: calculo }));
        if (response.status === 200) {
          this.setState({
            accesoSistema: response.data,
            txtEmpresa: JSON.parse(resp)['empresa'],
            txtNombre: JSON.parse(resp)['nombre'],
            txtnombreempresa: JSON.parse(resp)['nomemp'],
            logMenu: true
          })
        } else {
          alert('Usuario/Clave inválidas. Reingrese.');
          console.log(resp)
        }
      })
      .catch(response => {
        console.log("Error ========>", response);
      })
  }

  
  entradaMenu() {
    document.getElementById("sisAcceso").className = "nav-link invisible"
  }

  enterFunction = (e) => {
    if (e.keyCode === 13) {
      document.getElementById('btnAcceso').focus();
    }
  }

  limpiezaMenu() {
    //Anula elementos originales
    document.getElementById("sisAcceso").className = "nav-link active invisible";
    document.getElementById("contacto").className = "nav-link active invisible";
    document.getElementById("titulo").className = "nav-link active invisible";

    //Habilita menu
    document.getElementById("menuPrincipal").className = 'navbar-nav mx-lg-auto visible';
    document.getElementById("salir").className = 'nav-link visible';
  }

  render() {
    var logMenu = this.state.logMenu;

    return (
      <Router>
        <div>
          {!logMenu ?
            <div className="navbar navbar-expand-lg pt-1 navbar-dark bg-dark">
              <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span className="navbar-toggler-icon"></span>
              </button>
              <Link id="titulo" className="navbar-brand small" to="/">VerMer IT Solutions</Link>
              <div className="collapse navbar-collapse" id="navbarSupportedContent">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item active">
                    <Link id="sisAcceso" className="nav-link" data-toggle="modal" data-target="#exampleModal" to="/login">Sistema Ventas</Link>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                      <div className="modal-dialog">
                        <div className="modal-content">
                          <div className="modal-header navbar-dark bg-dark text-light">
                            <h5 className="modal-title" id="exampleModalLabel">Acceso al Sistema</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                            </button>
                          </div>
                          <div className="modal-body">
                            <label htmlFor="user">Usuario</label>
                            <div className="input-group input-group-sm mb-3">
                              <input id="user" type="text" name="txtUsuario" className="form-control" placeholder="Usuario" required value={this.state.txtUsuario} onChange={this.handleChange} />
                            </div>
                            <label htmlFor="pass">Clave</label>
                            <div className="input-group input-group-sm mb-3">
                              <input id="pass" type="password" name="txtPassword" className="form-control" placeholder="Password" required value={this.state.txtPassword} onChange={this.handleChange} onKeyPress={this.enterFunction} />
                            </div>
                          </div>
                          <div className="modal-footer bg-dark col-xs-10">
                            <button type="submit" id="btnAcceso" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.handleClick}>Acceder</button>
                          </div>
                        </div>
                      </div>
                    </div>
                  </li>
                  <li className="nav-item">
                    <Link id="contacto" className="nav-link" to="/">Contacto</Link>
                  </li>
                </ul>
              </div>
            </div>
            :
            <div>
              <Menusicomer
                acceso={this.state.accesoSistema}
                usuario={this.state.txtUsuario}
                nombre={this.state.txtNombre}
                empresa={this.state.txtEmpresa}
                nombreempresa={this.state.txtnombreempresa} />
            </div>
          }

        </div>
        <Route exact path="/menu" component={Menusicomer} />
      </Router>
    )
  }
}

export default App;
