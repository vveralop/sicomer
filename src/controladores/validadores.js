function reemplazaCommas(number) {
  return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

onkeypress = (event) => {
  const code = event.keyCode;
  console.log(code);
  if (code >= 48 && code <= 57 && code === 188 && code === 8 && code === 9) {
      return true;
  } else {
      return false;
  }
}

module.exports = { reemplazaCommas,  };