const imprimirElemento = (nempresa, encabezadoop, oper, opcion) => {
    var ventana = window.open('', '', 'height=400,width=100');
    ventana.document.write('<!DOCTYPE html>');
    ventana.document.write('<html>');
    ventana.document.write('<head>');
    ventana.document.write('<link rel="stylesheet" type="text/css" href="impresion.css" />');
    ventana.document.write('</head>');
    ventana.document.write('<body>');
    ventana.document.write('<div className="ticket">');
    ventana.document.write('<p className="centrado">' + nempresa + '</p>'); 
    ventana.document.write('<img src="images/busqueda.png" alt="Logotipo">');
    ventana.document.write('<p className="centrado">TICKET DE ' + encabezadoop.tipo + ' </p> ');
    ventana.document.write('<p className="centrado">Fecha: ' + encabezadoop.fecha + '</p>');
    ventana.document.write('<p className="centrado">Hora: ' + encabezadoop.hora + '</p>');
    ventana.document.write('<p className="centrado">N° Operación:   ' + oper + '</p>');
    ventana.document.write('<table className="table">');
    ventana.document.write('<hr color="black" size=2 width=34% align="left">');
    ventana.document.write('<thead>');
    ventana.document.write('<tr>');
    ventana.document.write('<th className="producto">Producto</th>');
    ventana.document.write('<th className="cantidad">#</th>');
    ventana.document.write('<th className="precio">Precio</th>');
    ventana.document.write('<th className="total">Total</th>');
    ventana.document.write('</tr>');
    ventana.document.write('</thead>');
    ventana.document.write('<tbody>');
    for (var i = 0; i < encabezadoop.lineas.length; i++) {
        ventana.document.write('<tr>');
        if (opcion === 1) {
            ventana.document.write('<td className="producto"> ' + encabezadoop.lineas[i]['nombre'] + '</td>');
        } else {
            ventana.document.write('<td className="producto"> ' + encabezadoop.lineas[i]['nombreproducto'] + '</td>');
        }
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['cantidad']) + '</td>');
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['precio']) + '</td>');
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['total']) + '</td>');
    }
    ventana.document.write('</tr>');
    ventana.document.write('</tbody>');
    ventana.document.write('</table>');

    ventana.document.write('<br><hr color="black" size=2 width=34% align="left"></br>');  
    ventana.document.write('<p>Total '+ encabezadoop.tipo + '           :   ' + encabezadoop.totalo + '</p>'); 

    ventana.document.write('<p className="centrado">¡GRACIAS POR SU '+ encabezadoop.tipo + '!<br>www.vermer.cl</br></p>');
    ventana.document.write('</div></body></html>');
    ventana.document.close();
    ventana.focus();
    ventana.print();
    ventana.close();
    return true;
}

module.exports = { imprimirElemento };