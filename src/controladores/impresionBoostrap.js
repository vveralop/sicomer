const imprimirElemento = (nempresa, encabezadoop, oper, opcion) => {
    const fecha = new Date();
    var ventana = window.open('', '', 'height=400,width=100');
    ventana.document.write('<!DOCTYPE html>');
    ventana.document.write('<html>');
    ventana.document.write('<head>');
    ventana.document.write('<link rel="stylesheet" href="bootstrap/dist/css/bootstrap.min.css"/>');
    ventana.document.write('<script src="bootstrap/dist/js/bootstrap.min.js"></script>')

    ventana.document.write('</head>');
    ventana.document.write('<body>');
    ventana.document.write('<div className="ticket">');
    ventana.document.write('<img src="images/busqueda.png" alt="Logotipo">');
    ventana.document.write('<p className="centrado">' + nempresa +
        '<br>Valparaíso</br> Fecha:' +
        (fecha.getDate() + "-" + fecha.getMonth() + "-" + fecha.getFullYear()) +
        '<br>Hora: ' + fecha.getTime() + '</br></p>');
    ventana.document.write('<p className="centrado">' + oper + '</p>');
    ventana.document.write('<table className="table">');
    ventana.document.write('<thead>');
    ventana.document.write('<tr>');
    ventana.document.write('<th scope="col" className="producto">Producto</th>');
    ventana.document.write('<th scope="col" className="cantidad">#</th>');
    ventana.document.write('<th scope="col" className="precio">Precio</th>');
    ventana.document.write('<th scope="col" className="total">Total</th>');
    ventana.document.write('</tr>');
    ventana.document.write('</thead>');
    ventana.document.write('<tbody>');
    for (var i = 0; i < encabezadoop.lineas.length; i++) {
        ventana.document.write('<tr>');
        if (opcion === 1) {
            ventana.document.write('<td className="producto"> ' + encabezadoop.lineas[i]['nombre'] + '</td>');
        } else {
            ventana.document.write('<td className="producto"> ' + encabezadoop.lineas[i]['nombreproducto'] + '</td>');
        }
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['cantidad']) + '</td>');
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['precio']) + '</td>');
        ventana.document.write('<td className="cantidad"> ' + Intl.NumberFormat("de-DE").format(encabezadoop.lineas[i]['total']) + '</td>');
    }
    ventana.document.write('</tr>');
    ventana.document.write('<hr color="black" size=2 width=32% align="left" />');
    ventana.document.write('</tbody>');
    ventana.document.write('</table>');
    ventana.document.write('<p className="centrado">¡GRACIAS POR SU COMPRA!<br>www.vermer.cl</p>');
    ventana.document.write('</div></body></html>');
    ventana.document.close();
    ventana.focus();
    ventana.print();
    ventana.close();
    return true;
}

module.exports = { imprimirElemento };