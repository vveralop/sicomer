import React from 'react';
import { useBarcode } from '@createnextapp/react-barcode';

function numberFormat (num) {
    return (Intl.NumberFormat("de-DE").format(num));
}
 
function codigoB (dato) {
  const { inputRef } = useBarcode({
    value: dato,
    options: {
      displayValue: false,
      background: '#ffc0cb',
    }
  });
  
  return <canvas ref={inputRef} />;
};

module.exports =   { numberFormat, codigoB };