import React from 'react';
import ReactDOM from 'react-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
//import App from './Att';
import App from './App';
import { BrowserRouter as Router } from 'react-router-dom';

ReactDOM.render(
   <Router>
      <App />
      <footer className="bg bg-dark">
         <div className="container">
            <h4 className="small text-center text-light">2020 © Vermer IT Solutions</h4>
         </div>
      </footer>

   </Router>,
   document.getElementById('root'));