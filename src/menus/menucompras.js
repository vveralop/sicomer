import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Menuventas from './menuventas'

export default class Menucompras extends Component {
    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mx-auto">
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownOpe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Operaciones</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownOpe">
                                        <Link className="dropdown-item" to="/Compras">Compras</Link>
                                        <Link className="dropdown-item" to="/ventas">Ventas</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownCon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Consultas</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownCon">
                                        <Link className="dropdown-item" to="/qope">Operaciones del Día</Link>
                                        <Link className="dropdown-item" to="/qstock">Stock de Productos</Link>
                                        <Link className="dropdown-item" to="/qflujoc">Flujo de Caja</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownPar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Parametría</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownPar">
                                        <Link className="dropdown-item" to="/users">Usuarios</Link>
                                        <Link className="dropdown-item" to="/produc">Productos</Link>
                                        <Link className="dropdown-item" to="/precios">Precios</Link>
                                        <Link className="dropdown-item" to="/empresa">Empresa</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownPro" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Procesos</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownPro">
                                        <Link className="dropdown-item" to="/inicio">Inicio de Día</Link>
                                        <Link className="dropdown-item" to="/cierre">Cierre de Día</Link>
                                    </div>
                                </li>
                            </ul>
                            <li className="nav-item">
                                <Link className="text-light" to="/">Salir <span className="sr-only">(current)</span></Link>
                            </li>

                        </div>
                    </nav>
                </div>

                <Route path="/ventas" exact component={Menuventas} />
                <Route path="/compras" exact component={Menucompras} />
            </Router>
        )
    }
}
