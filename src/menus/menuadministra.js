const menuadm = () => {
    return (
        <div className="navbar navbar-expand-lg navbar-dark bg-dark text-light">
            {this.entradaMenu()}
            <h4 id="tituloempresa" className="navbar-brand">VerMer IT Solutions</h4>
            <ul id="menuSistema" className="nav nav-pills mx-auto visible">
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Operaciones</a>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/compras",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Compras</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/ventas",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Ventas</Link>
                        <div className="dropdown-divider"></div>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/opgestion",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Gestión de Operaciones</Link>

                    </div>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Gestión</a>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/qstock",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Consulta de Stock</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/qproductos",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Consulta de Productos</Link>
                    </div>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Parametría</a>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/tproductos",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantiene Tipos de Productos</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/productos",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantiene Productos</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/proveedores",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantiene Proveedores</Link>
                    </div>
                </li>
                <li className="nav-item dropdown">
                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Usuarios</a>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/cambioclave",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario
                                }
                            }}>Cambio de Clave</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/inicio",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Inicio Día</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/fin",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Fin Día</Link>
                    </div>
                </li>
                <li className="nav-item dropdown" id="administrador">
                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Clientes</a>
                    <div className="dropdown-menu">
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/empresas",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantención Empresas</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/users",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantiene Usuarios</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/secuencias",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Mantiene Secuencias</Link>
                        <Link className="dropdown-item text-success"
                            to={{
                                pathname: "/menu/dashboard",
                                state: {
                                    acceso: this.state.accesoSistema,
                                    usuario: this.state.txtUsuario,
                                    nombre: this.state.txtNombre,
                                    empresa: this.state.txtEmpresa
                                }
                            }}>Cuadro de Mando</Link>
                    </div>
                </li>
)
}

export.default {menuadm}