import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Users from './opciones/users';

export default class Menuusuario extends Component {
    constructor(props) {
        super(props);
        this.state = {
            tokenActivo: this.props.token,
            empresaActiva: this.props.empresa,
            nombreusuario: this.props.nombre,
            idusuario: this.props.usuario
        }
    }

    render() {
        return (
            <Router>
                <div>
                    <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
                        <div>
                        <Link className="text-center disabled">Usuario: {this.state.nombreusuario}</Link></div>
                        <div className="collapse navbar-collapse" id="navbarSupportedContent">
                            <ul className="navbar-nav mx-auto">
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownOpe" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Operaciones</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownOpe">
                                        <Link className="dropdown-item" to="/menuoperaciones/compras">Compras</Link>
                                        <Link className="dropdown-item" to="/menuoperaciones/ventas">Ventas</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownCon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Consultas</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownCon">
                                        <Link className="dropdown-item" to="/menuconsultas/qope">Operaciones del Día</Link>
                                        <Link className="dropdown-item" to="/menuconsultas/qstock">Stock de Productos</Link>
                                        <Link className="dropdown-item" to="/menuconsultas/qflujoc">Flujo de Caja</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownPar" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Parametría</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownPar">
                                        <Link className="dropdown-item" to="/menuparam/users">Usuarios</Link>
                                        <Link className="dropdown-item" to="/menuparam/productos">Productos</Link>
                                        <Link className="dropdown-item" to="/menuparam/precios">Precios</Link>
                                        <Link className="dropdown-item" to="/menuparam/empresa">Empresa</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <button type="button" className="btn btn-secondary dropdown-toggle text-light" id="navbarDropdownPro" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        Procesos</button>
                                    <div className="dropdown-menu" aria-labelledby="navbarDropdownPro">
                                        <Link className="dropdown-item" to="/menuprocesos/inicio">Inicio de Día</Link>
                                        <Link className="dropdown-item" to="/menuprocesos/cierre">Cierre de Día</Link>
                                    </div>
                                </li>
                                <Link id="salir" className="nav-link" to="/">Salir</Link>
                            </ul>
                        </div>
                    </nav>
                </div>

                <Route path="/menuparam/users" exact component={Users} />
            </Router>
        )
    }
}
