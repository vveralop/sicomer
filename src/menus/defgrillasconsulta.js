import React from 'react'

function priceFormatter(cell, row) {
    if (row.onSale) {
      return (
        <span>
          <strong style={ { color: 'black' } }>$ { Intl.NumberFormat("de-DE").format(cell) } </strong>
        </span>
      );
    }
  
    return (
      <span>$ { Intl.NumberFormat("de-DE").format(cell) }</span>
    );
}

function redondeo (num, decimalPlaces) {
    var d = decimalPlaces || 0;
    var m = Math.pow(10, d);
    var n = +(d ? num * m : num).toFixed(8); // Avoid rounding errors
    var i = Math.floor(n), f = n - i;
    var e = 1e-8; // Allow for rounding errors in f
    var r = (f > 0.5 - e && f < 0.5 + e) ?
        ((i % 2 === 0) ? i : i + 1) :
        Math.round(n);

    return d ? r / m : r;
}


const customTotal = (from, to, size) => (
    <div className="col pl-0">
        <span className="react-bootstrap-table-pagination-total small">
    </span>
    </div>
);

const pageButtonRenderer = ({
    page,
    active,
    disable,
    title,
    onPageChange
}) => {
    const handleClick = (e) => {
        e.preventDefault();
        onPageChange(page);
    };

    const activeStyle = {};
    
    if (active) {
        activeStyle.backgroundColor = 'black';
        activeStyle.color = 'white';
        activeStyle.fontSize = '14px';
        activeStyle.padding = '1px 1px 2px 2px';
    } else {
        activeStyle.backgroundColor = 'grey';
        activeStyle.color = 'black';
        activeStyle.fontSize = '14px';
        activeStyle.padding = '1px 1px 2px 2px';
    }

    if (typeof page === 'string') {
        activeStyle.backgroundColor = 'white';
        activeStyle.color = 'black';
        activeStyle.fontSize = '14px';
        activeStyle.padding = '1px 1px 2px 2px';
    }

    return (
        <li className="page-item">
            <a href="/#" onClick={handleClick} style={activeStyle}>{page}</a>
        </li>
    );
};

var options = {
    pageButtonRenderer,
    paginationSize: 10,
    pageStartIndex: 1,
    //alwaysShowAllBtns: true, // Always show next and previous button
    //withFirstAndLast: false, // Hide the going to First and Last page button
    hideSizePerPage: true, // Hide the sizePerPage dropdown always
    //hidePageListOnlyOnePage: true, // Hide the pagination list when only one page
    firstPageText: '<<',
    prePageText: '<',
    nextPageText: '>',
    lastPageText: '>>',
    nextPageTitle: 'First page',
    prePageTitle: 'Pre page',
    firstPageTitle: 'Next page',
    lastPageTitle: 'Last page',
    showTotal: "trues",
    sizePerPage: 15,
    paginationTotalRenderer: customTotal
};

const tittlesC = {
    width: "15px",
    height: "auto",
    margin: "2px 50px",
    padding: "2px",
    text: "right",
    grid: "auto",
    justify: "right",
    fontSize: "12px"
};

const filteres = {
    color: "black",
    fontSize: "12px",
    height: "0px",
    width: "120px",
    padding: "10px 10px 10px 10px"
}

const filteresF = {
    color: "black",
    fontSize: "12px",
    height: "20px",
    width: "80px",
    padding: "0px"
}

export { filteres, filteresF, tittlesC, options,  priceFormatter, redondeo };
