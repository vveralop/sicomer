import React, { Component } from 'react'
import axios from 'axios';
import jwt from 'jsonwebtoken';
import base64url from 'base64url';
import { data, calculo, urlLogin } from '../../configuraciones/data';
const secret = base64url.encode(Buffer.from(data), 'utf8');
const iva = 0.19

const detalle = []

export class ventas extends Component {
    constructor(props) {
        super()

        this.state = {
            prod: [],
            tpag: [],
            txtIdusuario: '',
            txtNombreusuario: '',
            txtNombreEmpTit: '',
            txtToken: '',

            txtcodigo: '',
            txtnombre: '',
            txtcantidad: 0,
            txtprecio: 0,
            txttotal: 0,
            txtfechaventa: '',
            txtfpago: 'EFECTIVO',
            txtvalordscto: 0,
            txtvaloriva: 0,
            txtvalorneto: 0
        }
        this.onChangelineaD = this.onChangelineaD.bind(this);
        this.onChangelineaC = this.onChangelineaC.bind(this);
        this.onselectProducto = this.onselectProducto.bind(this);
    }

    componentDidMount = () => {
        this.iniciaIngreso();
        document.getElementById('idproducto').focus();
    }

    iniciaIngreso() {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;
        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuario: usuario });
        this.setState({ txtNombreusuario: nombre });
        this.setState({ txtNombreEmpTit: empresa });
        //limpia arreglo
        detalle.splice(0, detalle.length);
        document.getElementById('tneto').setAttribute('readOnly', false);
        document.getElementById('tiva').setAttribute('readOnly', false);
        document.getElementById('totalf').setAttribute('readOnly', false);
        const fecha = new Date;

        this.setState({
            txtvalorneto: 0,
            txtvaloriva: 0,
            txtvalordscto: 0,
            txttotal: 0,
            txtfechaventa: fecha.getDate() + "-" + (fecha.getMonth() + 1) + "-" + fecha.getFullYear(),
            txtfpago: 'EFECTIVO'
        })

        //        document.getElementById('fpago').value = 'EFECTIVO'
        this.onselectProducto(empresa, acceso);
    }

    onselectProducto = async (empre, tok) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/ventas'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/ventas?Z=' + tokenDatos, headers)
        this.setState({ prod: res.data })
    }

    onselectTpago = async () => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/ventas/tpago'
        }

        const tokenDatos = this.codificaToken({
            idempresa: this.state.txtNombreEmpTit,
            token: this.state.txtToken
        });

        const res = await axios.get(urlLogin + '/menu/ventas/tpago?Z=' + tokenDatos, headers)
        this.setState({ tpag: res.data })
    }

    prodList() {
        if (this.state.prod === null) {
            return (null)
        } else {
            return (
                this.state.prod.map((prod) => (
                    <option className="small" key={prod.idproducto} value={prod.idproducto}>
                        [Código: {prod.idproducto} ] {prod.nombreproducto} [Stock: {prod.cantidad} {prod.unidadmedida}]
                    </option>)
                )
            )
        }
    }

    tpagoList() {
        if (this.state.tpag === null) {
            return (null)
        } else {
            return (
                this.state.tpag.map((tpag) => (
                    <option className="small" key={tpag.idtipopago} value={tpag.idtipopago}>
                        {tpag.idtipopago} - {tpag.definicion}
                    </option>)
                )
            )
        }
    }

    onChangelineaD = (e) => {
        const valor = this.numberFormat(e.target.value, 0);
        this.setState({ [e.target.name]: valor });
        //this.setState({ [e.target.name]: e.target.value });
    }

    onChangelineaC = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    limpiaArreglo = () => {
        document.getElementById('idproducto').value = '';
        document.getElementById('nombreprod').value = '';
        document.getElementById('cantidad').value = 0;
        document.getElementById('precio').value = 0;
        document.getElementById('total').value = 0;

        this.setState((state) => ({
            txtcodigo: '',
            txtnombre: '',
            txtcantidad: 0,
            txtprecio: 0,
            txttotal: 0,
            txtfpago: 'EFECTIVO'
        }));
    }

    agregarLinea = () => {
        if (document.getElementById('idproducto').value === '' ||
            document.getElementById('nombreprod').value === '' ||
            parseFloat(document.getElementById('cantidad').value) <= 0 ||
            parseFloat(document.getElementById('precio').value) <= 0 ||
            parseFloat(document.getElementById('total').value) <= 0) {
            alert('Debe ingresar todos los datos del producto comprado (No se permiten valores negativos en cantidad o Precio).')
        } else {
            const file = {
                codigo: document.getElementById('idproducto').value,
                nombre: document.getElementById('nombreprod').value,
                cantidad: Number(document.getElementById('cantidad').value),
                precio: Number(document.getElementById('precio').value),
                ivalinea: this.redondear((document.getElementById('total').value * iva), 0),
                total: this.redondear(Number(document.getElementById('total').value), 0)
            }
            detalle.push(file);
            this.limpiaArreglo();
            document.getElementById('idproducto').focus();
            this.calculaResumen(detalle, 0);
        }
    }

    eliminaLinea = (e) => {
        const registro = e.target.value;
        if (registro !== -1) {
            detalle.splice(registro, 1);
            this.limpiaArreglo();
            document.getElementById('idproducto').focus();
            this.calculaResumen(detalle, 0);
        }
    }

    calculaResumen(arregloDetalle, dsctof) {
        var capital = 0;
        for (var i = 0; i < arregloDetalle.length; i++) {
            capital += parseInt(arregloDetalle[i]['total']);
        }
        capital = capital - dsctof;
        const tiva = this.redondear(capital * iva, 0);
        const tneto = this.redondear(capital * (1 - iva), 0);

        // this.setState({ txttotal: this.numberFormat(capital) });
        // this.setState({ txtvaloriva: this.numberFormat(tiva) });
        // this.setState({ txtvalorneto: this.numberFormat(tneto) });
        document.getElementById('totalf').value = this.numberFormat(capital);
        document.getElementById('tiva').value = this.numberFormat(tiva);
        document.getElementById('tneto').value = this.numberFormat(tneto);
    }

    itemEncabezadoLista() {
        return (
            <tr>
                <td className="small col-xl-3 text-start">Código</td>
                <td className="small col-xl-4 text-start">Nombre</td>
                <td className="small col-xl-2 text-center">Cantidad</td>
                <td className="small col-xl-2 text-center">Precio</td>
                <td className="small col-xl-1 text-center">Total</td>
            </tr>
        )
    }

    itemLista() {
        if (detalle === null) {
            return (null)
        } else {
            return (
                detalle.map((lis, index) => (
                    <tr key={index}>
                        <td className="small col-xl-3">{lis.codigo}</td>
                        <td className="small col-xl-4">{lis.nombre}</td>
                        <td className="small col-xl-2 text-end">{this.numberFormat(lis.cantidad)}</td>
                        <td className="small col-xl-2 text-end">{this.numberFormat(lis.precio)}</td>
                        <td className="small col-xl-1 text-end">{this.numberFormat(lis.total)}</td>
                        <td className="small col-xl-1">
                            <button type="submit"
                                name="botonModal"
                                className="small btn btn-danger text-small"
                                value={index}
                                data-whatever="vm" onClick={this.eliminaLinea}></button>
                        </td>
                    </tr>)))
        }
    }

    enterFunction = (event) => {
        if (event.keyCode === '13') {
            alert('Presiono enter')
        }
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));
        return tokenRet;
    }

    revisaSiexisteElemento = (prd) => {
        if (this.state.prod !== null) {
            const arregloProd = this.state.prod;
            const result = (arregloProd.filter(prod => (prod.idproducto === prd)))

            if (result.length > 0) {
                return true;
            } else { return false }
        } else {
            return false;
        }
    }

    numberFormat(num, decimals) {
        return (Intl.NumberFormat("de-DE").format(num));
        //        var numParts = num.toString().split('.');
        //        numParts[0] = numParts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ',');
        //        return numParts.join('.');
    }

    traeDatos = (e) => {
        const res = this.revisaSiexisteElemento(e.target.value);

        if (res) {
            const arreglo = this.state.prod;
            const prod = arreglo.map(function (produc) {
                if (produc.idproducto === e.target.value) {
                    document.getElementById('idproducto').value = produc.idproducto;
                    document.getElementById('nombreprod').value = produc.nombreproducto;
                    document.getElementById('cantidad').value = 1;
                    document.getElementById('precio').value = produc.precioref;
                    document.getElementById('total').value =
                        document.getElementById('precio').value *
                        document.getElementById('cantidad').value;
                    document.getElementById('cantidad').focus();
                }
            })
        } else {
            if (document.getElementById('idproducto').value !== '') {
                alert('Producto inexistente, reingrese.');
                document.getElementById('idproducto').value = '';
                document.getElementById('nombreprod').value = '';
                document.getElementById('cantidad').value = '';
                document.getElementById('precio').value = '';
                document.getElementById('total').value = '';
                document.getElementById('idproducto').focus();
            }
        }
    }

    grabaOperacion = async () => {
        if (detalle.length > 0) {
            if (this.state.txttotal === 0 ||
                this.state.txtvalorneto === 0 ||
                this.state.txtfpago === '' ||
                this.state.txtvaloriva === '0' ||
                this.state.txtfechaventa === '') {
                alert('Faltan datos para grabar operación.')
            } else {
                const encabezado = JSON.stringify({
                    idempresa: this.state.txtNombreEmpTit,
                    fechaventa: this.state.txtfechaventa,
                    totalventa: Number(document.getElementById('totalf').value.replace(/\./g, "")), //Number(this.state.txttotal),
                    formapagovta: this.state.txtfpago,
                    ivaventa: Number(document.getElementById('tiva').value.replace(/\./g, "")),   //Number(this.state.txtvaloriva),
                    totalnetovta: Number(this.state.txtvalorneto),
                    desctocpa: Number(this.state.txtvalordscto),
                    lineas: detalle
                });

                const headers = {
                    'Content-Type': 'application/json',
                    'Access-Control-Allow-Origin': 'http://localhost:5000/menu/ventas'
                }

                const tokenDatos = this.codificaToken({
                    idempresa: this.state.txtNombreEmpTit,
                    idusuario: this.state.txtIdusuario,
                    token: this.state.txtToken,
                    datos: encabezado
                });

                const res = await axios.post(urlLogin + '/menu/ventas', { body: tokenDatos }, headers)

                if (res.status === 200) {
                    alert('Operacion generada con folio: ' + res.data['seq']);
                    this.iniciaIngreso();
                    document.getElementById('idproducto').focus();
                } else {
                    alert('Ha habido un error. Avise al Administrador: ' + res.data['registro']);
                }
            }
        } else {
            alert('Sin registros para grabar. Ingrese productos');
            document.getElementById('idproducto').focus();
        }
    }
    recalcula = () => {
        const arregloProd = this.state.prod;
        const prod = arregloProd.filter((produc) => {
            if (produc.idproducto === document.getElementById('idproducto').value &&
                produc.cantidad >= document.getElementById('cantidad').value) {
                return true;
            } else {
                return false;
            }
        })

        if (prod.length < 1) {
            alert('No se puede vender más de la cantidad del stock. Revise el Stock y vuelva a ingresar.');
            document.getElementById('cantidad').focus();
        } else {
            const valortot = document.getElementById('precio').value *
                document.getElementById('cantidad').value;
            document.getElementById('total').value = valortot;
            this.setState({ txttotal: valortot });
        }
    }

    redondear(num, dec) {
        const p = 10 ** dec;
        const fpFix = Number.EPSILON * (num < 0 ? -1 : 1);
        return Math.round((num + fpFix) * p) / p;
    }

    render() {
        return (
            <div>
                <div className="container-fluid text-center bg-dark text-light">Ingreso de Ventas</div>
                <div className="container-fluid text-center small bg-dark text-success">(Usuario: {this.state.txtNombreusuario})</div>
                <div className="container-fluid row px-0">
                    <div className="row text-light bg-dark text-center col-md-5 px-0">
                        <div className="container text-light bg-dark col-md-10 border">Ingresar Productos Ventas</div>
                        <div className="container pt-3">
                            <div className="row pl-5 fs-6">
                                <label className="row small bg-dark col-md-3 pl-3" htmlFor="idproducto">Código Producto</label>
                                <input className="small col-md-6" name="txtcodigo" type="text" id="idproducto" placeholder="Código" maxLength="40" onChange={this.onChangelineaC} onBlur={this.traeDatos} />
                                <select name="txtcodigo" id="selIdpro"
                                    className="small form-select col-md-3"
                                    onChange={this.traeDatos}
                                    value={this.state.txtcodigo}>
                                    <option>Buscar</option>
                                    {this.prodList()}
                                </select>
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="nameprod">Producto</label>
                                <input className="small col-md-9" name="txtnombre" type="text" id="nombreprod" placeholder="Nombre producto" maxLength="50" onChange={this.traeDatos} readOnly />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-right pl-3" htmlFor="cantidad">Cantidad</label>
                                <input className="small col-md-3 text-left" name="txtcantidad" type="text" id="cantidad" maxLength="10" placeholder="Cantidad" onChange={this.onChangelineaD} onBlur={this.recalcula} />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="precio">Precio</label>
                                <input className="small col-md-3" name="txtprecio" type="text" id="precio" maxLength="10" placeholder="Precio" onChange={this.onChangelineaD} onBlur={this.recalcula} />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="precio">Total</label>
                                <input className="small col-md-3" name="txttotalope" type="text" id="total" maxLength="10" placeholder="Total" readOnly />
                            </div>
                            <div className="container pt-1">
                                <button className="btn btn-success btn-sm border mx-auto" onClick={this.agregarLinea}>Agregar Venta</button>
                            </div>
                            <div className="px-6 pt-2">
                                <div className="col container-fluid text-light bg-dark col-md-10 pb-auto border">Total Venta</div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tneto">Total Neto</label>
                                    <input className="small col-md-3 text-right" name="txtvalorneto" value={this.state.txtvalorneto} maxLength="40" id="tneto" type="text" onChange={this.onChangelineaD} readOnly />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tiva">Iva</label>
                                    <input className="small col-md-3 text-right" name="txtvaloriva" value={this.state.txtvaloriva} maxLength="40" id="tiva" type="text" onChange={this.onChangelineaD} readOnly />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tdesc">Dscto</label>
                                    <input className="small col-md-3 text-right" name="txtvalordscto" value={this.state.txtvalordscto} maxLength="40" id="tdesc" type="text" readOnly onChange={this.onChangelineaD} readOnly />
                                </div>
                                <div>
                                    <label className="small text-left text-light text-bold col-md-4 bg-dark" htmlFor="total">Total de Esta Venta</label>
                                    <input className="small col-md-3 text-right" name="txttotal" value={this.state.txttotal} maxLength="40" id="totalf" type="text" onChange={this.onChangelineaD} readOnly />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="fventa">Fecha Compra</label>
                                    <input className="small col-md-3 text-right" name="txtfechaventa" value={this.state.txtfechaventa} maxLength="40" id="fventa" type="text" onChange={this.onChangelineaC} />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="fpago">Forma de Pago</label>
                                    <select name="txtfpago" id="fpago"
                                        value={this.state.txtfpago}
                                        className="small col-md-3 text-left"
                                        onChange={this.onChangelineaC}
                                        onClick={this.onselectTpago}>
                                        <option selected="selected">EFECTIVO</option>
                                        {this.tpagoList()}
                                    </select>
                                </div>
                            </div>
                            <button className="btn btn-success btn-sm border">Eliminar</button>
                            <button className="btn btn-success btn-sm border" onClick={this.grabaOperacion}>Grabar Venta</button>
                            <button className="btn btn-success btn-sm border">Imprimir Detalle</button>
                        </div>
                    </div>
                    <div className="col col-xl-5 pl-3">
                        <div className="border text-light bg-dark text-center">Detalle de Venta</div>
                        <div className="table table-sm table-striped">
                            <div>
                                {this.itemEncabezadoLista()}
                            </div>
                            <div className="col small text-dark align-items-start">
                                {this.itemLista()}
                            </div>
                        </div>
                    </div>
                </div>
            </div >
        )
    }
}

export default ventas
