import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export class Secuencias extends Component {
    constructor(props) {
        super()
        this.state = {
            seq: [],
            select: [],
            txtIdusuarioTit: '',
            txtNombreEmpTit: '',
            txtNombreusuarioTit: '',
            txtToken: '',

            txtidEmpresa: '',
            txtnombreEmpresa: '',
            txtnombreSeq: '',
            txtValor: ''
        }
        this.componentDidMount = this.componentDidMount.bind(this);
        this.lecturaInicial = this.lecturaInicial.bind(this);
        this.limpiaDatos = this.limpiaDatos.bind(this);
    }

    componentDidMount = (e) => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuarioTit: usuario });
        this.setState({ txtNombreusuarioTit: nombre });
        this.setState({ txtNombreEmpTit: empresa });
        this.lecturaInicial(empresa, acceso, usuario);
    }

    onClickGrabaSeq = async (e) => {
        if (this.state.txtidEmpresa === '' || this.state.txtnombreSeq === '' || this.state.txtValor === '') {
            alert('Todos los campos deben ser completados.')
        } else {
            const tokenDatos = this.codificaToken({
                idusuario: this.state.txtIdusuarioTit,
                idempresa: this.state.txtidEmpresa,
                sequencia: this.state.txtnombreSeq,
                valor: this.state.txtValor,
                token: this.state.txtToken
            });
            const res = await axios.post(urlLogin + '/menu/secuencias', { body: tokenDatos });
            if (res.status === 200) {
                alert('Registro Grabado Correctamente.')
                this.limpiaDatos();
                this.lecturaInicial(this.state.txtNombreEmpTit, this.state.txtToken);
                document.getElementById("btnBorrar").className = "col-xl-2 invisible"
            } else {
                alert('Ha habido un error. Avise al Administrador.');
            }
        }
    }

    onClickBorraSeq = async (e) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/secuencias'
        };

        const tokenDatos = this.codificaToken({
            idempresa: this.state.txtidEmpresa,
            idusuario: this.state.txtIdusuarioTit,
            sequencia: this.state.txtnombreSeq,
            token: this.state.txtToken
        });

        const res = await axios.delete(urlLogin + '/menu/secuencias?Z=' + tokenDatos, headers);
        if (res.status === 200) {
            alert('Registro Borrado Correctamente.')
            this.limpiaDatos();
            this.lecturaInicial(this.state.txtNombreEmpTit, this.state.txtToken);
        } else {
            alert('Ha habido un error. Avise al Administrador.');
        }
    }

    lecturaInicial = async (empr, tok, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/secuencias'
        };

        const tokenDatos = this.codificaToken({
            idempresa: empr,
            idusuario: user,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/secuencias?Z=' + tokenDatos, headers)
        this.setState({ seq: res.data })
        this.onClickSelecction();
        document.getElementById("btnBorrar").className = "btn btn-outline-success btn-sm my-2 my-sm-0";
    }

    empresasList(props) {
        return (
            this.state.select.map((empre) => (
                <option key={empre.idEmpresa} value={empre.idEmpresa}>
                    [Rut: {empre.idEmpresa} ] {empre.nombreEmpresa}
                </option>)))
    }

    sequList = (e) => {
        return (this.state.seq.map((sequ, index) => (
            <tr key={index}>
                <td className="active small">{sequ.idempresa}</td>
                <td className="active small">{sequ.nombreEmpresa}</td>
                <td className="active small">{sequ.nombresecuencia}</td>
                <td className="active small">{sequ.valor}</td>
                <td>
                    <button type="submit"
                        name="botonModal"
                        className="btn btn-danger"
                        data-toggle="modal"
                        data-target="#exampleModal"
                        value={index}
                        data-whatever="vm" onClick={this.onClickModal}></button>
                </td>
            </tr>)))
    }

    onClickModal = (e) => {
        const registro = e.target.value;
        const idempresa = this.state.seq[registro].idempresa;
        const nombre = this.state.seq[registro].nombreEmpresa;
        const sequencia = this.state.seq[registro].nombresecuencia;
        const valor = this.state.seq[registro].valor;

        this.onClickSelecction();

        this.setState({
            txtidEmpresa: idempresa,
            txtnombreEmpresa: nombre,
            txtnombreSeq: sequencia,
            txtValor: valor
        })

        this.labelModal('Modifica Secuencias');
    }

    onClickSelecction = async (e) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/secuencias/select'
        };

        const tokenDatos = this.codificaToken({
            idempresa: this.state.txtidEmpresa,
            idusuario: this.state.txtIdusuarioTit,
            token: this.state.txtToken
        });

        const res = await axios.get(urlLogin + '/menu/secuencias/select?Z=' + tokenDatos, headers)
        this.setState({ select: res.data })
    }

    cierraModal = (e) => {
        this.limpiaDatos();
    }

    escFunction = (e) => {
        if (e.keyCode === 27) {
            this.limpiaDatos();
        }
    }

    onClickCreaSeq = (e) => {
        this.limpiaDatos();
        this.labelModal('Creación de Secuencias');
    }

    labelModal = (texto) => {
        document.getElementById("labelcremod").innerHTML = texto;
    }

    limpiaDatos = () => {
        this.setState({
            txtidEmpresa: '',
            txtnombreEmpresa: '',
            txtnombreSeq: '',
            txtValor: 0
        })
    }

    onChangeNewSeq = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));

        return tokenRet;
    }

    render() {
        return (
            <Router>
                <div>
                    <div className="navbar navbar-dark bg-dark text-light">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Secuencias
                         <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
                        <div className="form-inline">
                            <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClickCreaSeq}>Crear Secuencia</button>
                        </div>
                    </div>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
                        <div className="modal-dialog modal-lg" role="document">
                            <div className="modal-content">
                                <div className="modal-header bg-dark text-white">
                                    <h5 className="modal-title" id="labelcremod">Modificar Secuencias</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="container">
                                    <form className="form-group">
                                        <div className="container">
                                            <label htmlFor="">Id - Empresa</label>
                                            <div className="row col-xs-8">
                                                <select name="txtidEmpresa" id="idempresa"
                                                    className="selectpicker form-control col-md-8"
                                                    value={this.state.txtidEmpresa}
                                                    onChange={this.onChangeNewSeq}>
                                                    <option aria-selected>Seleccione Empresa</option>
                                                    {this.empresasList()}
                                                </select>
                                            </div>

                                            <label htmlFor="">Nombre Secuencia</label>
                                            <div className="row col-xs-5">
                                                <input name="txtnombreSeq" maxLength="15" value={this.state.txtnombreSeq} id="seq" type="text" placeholder="Nombre secuencia" className="col-md-6" onChange={this.onChangeNewSeq} />
                                            </div>
                                            <label htmlFor="inputState">Valor</label>
                                            <div className="row col-xs-3">
                                                <input name="txtValor" value={this.state.txtValor} id="seqvalor" type="text" placeholder="valor secuencia" className="col-md-6" onChange={this.onChangeNewSeq} />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="modal-footer bg-dark">
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.onClickGrabaSeq}>Guardar</button>
                                    <button id="btnBorrar" type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.onClickBorraSeq}>Borrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="table-responsive-sm">
                        <table className="table table-striped table-sm">
                            <thead className="thead-dark table-sm">
                                <tr>
                                    <th scope="col" className="small font-weight-bold">Empresa</th>
                                    <th scope="col" className="small font-weight-bold">Nombre</th>
                                    <th scope="col" className="small font-weight-bold">Secuencia</th>
                                    <th scope="col" className="small font-weight-bold">Valor</th>
                                    <th scope="col" className="small font-weight-bold">Visor</th>
                                </tr>
                            </thead>
                            <tbody className="grid">
                                {this.sequList()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </Router>
        )
    }
}

export default Secuencias;
