import React, { Component } from 'react'
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export class tipoproducto extends Component {
  constructor(props) {
    super()
    this.state = {
      tprod: [],
      txtIdusuarioTit: '',
      txtNombreEmpTit: '',
      txtNombreusuarioTit: '',
      txtToken: '',

      txttipoProducto: '',
      txtdescripcion: '',
      txtusuarioCrea: '',
      txtfechaCrea: ''
    }
  }

  componentDidMount = (e) => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;

    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.setState({ txtNombreEmpTit: empresa });
    this.lecturaInicial(empresa, acceso, usuario);
  }

  onClicktProducto = (e) => {
    this.limpiaDatos();
    this.labelModal('Crear Tipos de Productos');
  }

  encabezaList = () => {
    return (<tr>
      <th scope="col" className="small font-weight-bold">Tipo Producto</th>
      <th scope="col" className="small font-weight-bold">Descripción</th>
      <th scope="col" className="small font-weight-bold">Usuario Crea</th>
      <th scope="col" className="small font-weight-bold">Fecha Crea</th>
      <th scope="col" className="small font-weight-bold">Acción</th>
    </tr>)
  }

  onClickGrabatprod = async (e) => {
    if (this.state.txttipoProducto === '' || this.state.txtdescripcion === '') {
      alert('Todos los campos deben ser completados.')
    } else {
      const tokenDatos = this.codificaToken({
        idusuario: this.state.txtIdusuarioTit,
        idempresa: this.state.txtNombreEmpTit,
        tipoproducto: this.state.txttipoProducto,
        descripcion: this.state.txtdescripcion,
        valor: this.state.txtValor,
        token: this.state.txtToken
      });
      const res = await axios.post(urlLogin + '/menu/tproductos', { body: tokenDatos });
      if (res.status === 200) {
        alert('Registro Grabado Correctamente.')
        this.limpiaDatos();
        this.lecturaInicial(this.state.txtNombreEmpTit, this.state.txtToken);
      } else {
        alert(res.data);
      }
    }
  }

  onClickBorratprod = async (e) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/tproductos'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtNombreEmpTit,
      tippro: this.state.txttipoProducto,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
    });
    const res = await axios.delete(urlLogin + '/menu/tproductos?Z=' + tokenDatos, headers);
    if (res.status === 200) {
      alert('Registro Borrado Correctamente.')
      this.limpiaDatos();
      this.lecturaInicial(this.state.txtNombreEmpTit, this.state.txtToken);
    } else {
      console.log(res.data);
      alert('Ha habido un error. Avise al Administrador.');
    }
  }

  lecturaInicial = async (empr, tok, user) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/tproductos'
    };

    const tokenDatos = this.codificaToken({
      idempresa: empr,
      idusuario: user,
      token: tok
    });

    const res = await axios.get(urlLogin + '/menu/tproductos?Z=' + tokenDatos, headers)
    this.setState({ tprod: res.data })
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  tprodList = (e) => {
    if (this.state.tprod !== null) {
      return (this.state.tprod.map((tp, index) => (
        <tr key={index}>
          <td className="active small">{tp.tipoproducto}</td>
          <td className="active small">{tp.descripcion}</td>
          <td className="active small">{tp.usuariocrea}</td>
          <td className="active small">{tp.fechacrea}</td>
          <td>
            <button type="submit"
              name="botonModal"
              className="btn btn-danger"
              data-toggle="modal"
              data-target="#exampleModal"
              value={index}
              data-whatever="vm" onClick={this.onClickModal}></button>
          </td>
        </tr>)))
    }
  }

  onClickModal = (e) => {
    const registro = e.target.value;
    const tproduc = this.state.tprod[registro].tipoproducto;
    const desctip = this.state.tprod[registro].descripcion;

    this.setState({
      txttipoProducto: tproduc,
      txtdescripcion: desctip
    })

    this.labelModal('Modifica Tipos de Producto');
  }

  labelModal = (texto) => {
    document.getElementById("labelcremod").innerHTML = texto;
  }

  cierraModal = (e) => {
    this.limpiaDatos();
  }

  escFunction = (e) => {
    if (e.keyCode === 27) {
      this.limpiaDatos();
    }
  }

  onClickCreaSeq = (e) => {
    this.limpiaDatos();
  }

  limpiaDatos = () => {
    this.setState({
      txttipoProducto: '',
      txtdescripcion: ''
    })
  }

  onChangeNewSeq = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Tipos de Productos
                  <p className="text-center text-light small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
            <div className="form-inline">
              <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClicktProducto}>Agregar Tipo Producto</button>
            </div>
          </div>
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content col-xs-10">
                <div className="modal-header bg-dark text-white">
                  <h5 className="modal-title" id="labelcremod">Modificar Tipo de Productos</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="container">
                  <form className="form-group">
                    <div className="container col-xs-10">
                      <label className="col-xl-3" id="labelprod" htmlFor="idproducto">Tipo Producto</label>
                      <label className="col-xl-7" htmlFor="nombreprod">Descripción</label>
                    </div>
                    <div className="row col-md-10">
                      <input name="txttipoProducto" maxLength="15" value={this.state.txttipoProducto} id="idproducto" type="text" placeholder="id producto" className="col-md-3" onChange={this.onChangeNewUser} />
                      <input name="txtdescripcion" maxLength="50" value={this.state.txtdescripcion} id="nombreprod" type="text" placeholder="Nombre producto" className="col-md-7" onChange={this.onChangeNewUser} />
                    </div>
                  </form>
                </div>
                <div className="modal-footer bg-dark col-xs-10">
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.onClickGrabatprod}>Guardar</button>
                  <button id="borrarTprod" type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.onClickBorratprod}>Borrar</button>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                {this.encabezaList()}
              </thead>
              <tbody className="grid">
                {this.tprodList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}

export default tipoproducto
