import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import CryptoJS from 'crypto-js';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class Users extends Component {
  constructor(props) {
    super()

    this.state = {
      users: [],
      select: [],
      txtIdusuarioTit: '',
      txtNombreEmpTit: '',
      txtNombreusuarioTit: '',
      txtToken: '',
      txtIdusuario: '',
      txtNombreusuario: '',
      txtCorreo: '',
      txtTelefono: '',
      txtRutempre: '',
      txtNombrempre: '',
      txtValidez: '',
      txtClave: ''
    }

    this.onClickModal = this.onClickModal.bind(this);
    this.cierraModal = this.cierraModal.bind(this);
    this.grabaUsuarios = this.grabaUsuarios.bind(this);
    this.limpiaDatos = this.limpiaDatos.bind(this);
    this.onClickSelecction = this.onClickSelecction.bind(this);
    this.onChangeNewUser = this.onChangeNewUser.bind(this);
    this.lecturaInicial = this.lecturaInicial.bind(this);
    this.usersList = this.usersList.bind(this);
  }

  componentDidMount = (e) => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;

    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.setState({ txtNombreEmpTit: empresa });
    this.onClickSelecction();
    this.lecturaInicial();
  }

  empresasList() {
    return (
      this.state.select.map((empre) => (
        <option key={empre.idEmpresa} value={empre.idEmpresa}>
          [Rut: {empre.idEmpresa} ] {empre.nombreEmpresa}
        </option>)))
  }

  OnClickClose = async (e) => {
    const res = await axios.post(urlLogin + '/salir', { idusuario: this.state.txtIdusuario });
    if (res.status === 200) {
      alert('Registro update Correctamente.')
      this.limpiaDatos();
    } else {
      alert('Ha habido un error al salir del sistema. Avise al Administrador.')
    }
  }

  encabezaList = () => {
    return (<tr>
      <th scope="col" className="small font-weight-bold">Usuario</th>
      <th scope="col" className="small font-weight-bold">Nombre</th>
      <th scope="col" className="small font-weight-bold">Empresa</th>
      <th scope="col" className="small font-weight-bold">Nombre Empresa</th>
      <th scope="col" className="small font-weight-bold">Correo</th>
      <th scope="col" className="small font-weight-bold">Teléfono</th>
      <th scope="col" className="small font-weight-bold">Estado</th>
      <th scope="col" className="small font-weight-bold">Usuario Crea</th>
      <th scope="col" className="small font-weight-bold">Fecha Creación</th>
      <th scope="col" className="small font-weight-bold">Visor</th>
    </tr>)
  }

  usersList = (e) => {
    return (this.state.users.map((user, index) => (
      <tr key={user.idusuario}>
        <td className="active small">{user.idusuario}</td>
        <td className="active small">{user.nombreUsuario}</td>
        <td className="active small">{user.idempresa}</td>
        <td className="active small">{user.nombreempresa}</td>
        <td className="active small">{user.correoUsuario}</td>
        <td className="active small">{user.telefonoUsuario}</td>
        <td className="active small">{user.validaidcli}</td>
        <td className="active small">{user.usuarioCrea}</td>
        <td className="active small">{user.fechacrea}</td>
        <td>
          <button type="submit"
            name="botonModal"
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#exampleModal"
            value={index}
            data-whatever="vm" onClick={this.onClickModal}></button>
        </td>
      </tr>)))
  }

  onChangeRadio = (e) => {
    this.setState({ txtValidez: e.target.value })
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onClickCreauser = (e) => {
    this.onClickSelecction();
    document.getElementById('idusuario').removeAttribute('readOnly');
    document.getElementById('gridRadios1').setAttribute('checked', true);

    this.labelModal('Crea Datos de Usuarios');
  }

  grabaUsuarios = async (e) => {
    //valida que no falte ningún campo
    if (this.state.txtIdusuario === '' || this.state.txtNombreusuario === '' || this.state.txtCorreo === '' ||
      this.state.txtTelefono === '' || this.state.txtRutempre === '' || this.state.txtValidez === '') {
      alert('Todos los campos deben ser completados.')
    } else {
      const ttoClave = CryptoJS.HmacSHA256(this.state.txtClave, secret).toString();
      this.setState({ txtClave: ttoClave });
      const tokenDatos = this.codificaToken({
        idusuario: this.state.txtIdusuario,
        nombreusuario: this.state.txtNombreusuario,
        correousuario: this.state.txtCorreo,
        idempresa: this.state.txtRutempre,
        telefonousuario: this.state.txtTelefono,
        claveusuario: ttoClave,
        validausuario: this.state.txtValidez,
        iduser: this.state.txtIdusuarioTit,
        token: this.state.txtToken
      });

      const res = await axios.post(urlLogin + '/menu/users', { body: tokenDatos });
      if (res.status === 200) {
        alert('Registro Insertado Correctamente.')
        this.limpiaDatos();
        this.lecturaInicial();
      } else {
        alert('Ha habido un error al grabar datos. Avise al Administrador.');
      }
    }
  }

  lecturaInicial = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/users'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtNombreEmpTit,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/users?Z=' + tokenDatos, headers)
    this.setState({ users: res.data })
  }

  onClickSelecction = async (e) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/users/select'
    };

    const tokenDatos = this.codificaToken({
      idemp: this.state.txtNombreEmpTit,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
  });

    const res = await axios.get(urlLogin + '/menu/users/select?Z=' + tokenDatos, headers)
    this.setState({ select: res.data })
  }

  onClickModal = (e) => {
    const registro = e.target.value;
    const idusuario = this.state.users[registro].idusuario;
    const nombreusu = this.state.users[registro].nombreUsuario;
    const correousu = this.state.users[registro].correoUsuario;
    const telefousu = this.state.users[registro].telefonoUsuario;
    const idemprusu = this.state.users[registro].idempresa;
    const nomemprus = this.state.users[registro].nombreempresa;
    const claveusua = this.state.users[registro].claveUsuario;
    const validaidc = this.state.users[registro].validaidcli;

    this.onClickSelecction();

    this.setState({
      txtIdusuario: idusuario,
      txtNombreusuario: nombreusu,
      txtCorreo: correousu,
      txtTelefono: telefousu,
      txtRutempre: idemprusu,
      txtNombrempre: nomemprus,
      txtValidez: validaidc,
      txtClave: claveusua
    })
    this.labelModal('Modifica Datos de Usuarios');
    if (validaidc === "S") {
      document.getElementById('gridRadios1').setAttribute('checked', true);
      document.getElementById('gridRadios2').removeAttribute('checked');
    } else {
      document.getElementById('gridRadios2').setAttribute('checked', true);
      document.getElementById('gridRadios1').removeAttribute('checked');
    }
    document.getElementById('idusuario').setAttribute('readOnly', true);
  }

  cierraModal = (e) => {
    this.limpiaDatos();
  }

  escFunction = (e) => {
    if (e.keyCode === 27) {
      this.limpiaDatos();
    }
  }

  labelModal = (texto) => {
    document.getElementById("labelcremod").innerHTML = texto;
  }

  limpiaDatos = () => {
    this.setState({
      txtIdusuario: '',
      txtNombreusuario: '',
      txtCorreo: '',
      txtTelefono: '',
      txtRutempre: '',
      txtNombrempre: '',
      txtValidez: '',
      txtClave: ''
    })
  }

  // validaToken = async (tok) => {
  //   const headers = {
  //     'Content-Type': 'application/json',
  //     'Access-Control-Allow-Origin': 'http://localhost:5000/menu/users'
  //   };

  //   const res = await axios.get(urlLogin + '/menu/users/valida?valtok=' + tok, headers);
  //   return (res.data)
  // }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Usuarios
            <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
            <div className="form-inline">
              <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClickCreauser}>Crear Usuario</button>
            </div>
          </div>
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content">
                <div className="modal-header bg-dark text-white">
                  <h5 className="modal-title" id="labelcremod">Modificar Datos del Usuario</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="container">
                  <form className="form-group">
                    <div className="container">
                      <div className="row col-xs-12">
                        <label className="col-md-3" htmlFor="idusuario">Id</label>
                        <label className="col-md-9" htmlFor="nombreus">Nombre de Usuario</label>
                      </div>
                      <div className="row col-xs-12">
                        <input name="txtIdusuario" maxLength="15" value={this.state.txtIdusuario} id="idusuario" type="text" placeholder="id user" className="col-md-3" onChange={this.onChangeNewUser} />
                        <input name="txtNombreusuario" maxLength="50" value={this.state.txtNombreusuario} id="nombreus" type="text" placeholder="Nombre" className="col-md-9" onChange={this.onChangeNewUser} />
                      </div>
                      <div className="row col-xs-12">
                        <label className="col-md-6" htmlFor="correo">Correo</label>
                        <label className="col-md-3" htmlFor="fono">Teléfono</label>
                      </div>
                      <div className="row col-xs-12">
                        <input name="txtCorreo" maxLength="40" value={this.state.txtCorreo} id="correo" type="text" placeholder="Correo" className="col-md-6" onChange={this.onChangeNewUser} />
                        <input name="txtTelefono" maxLength="25" value={this.state.txtTelefono} id="fono" type="text" placeholder="Fono" className="col-md-3" onChange={this.onChangeNewUser} />
                      </div>
                      <div className="row col-xs-12">
                        <label htmlFor="selempresa">Empresa</label>
                      </div>
                      <div className="row col-xs-12">
                        <select name="txtRutempre" id="selempresa"
                          className="selectpicker form-control col-md-12"
                          value={this.state.txtRutempre}
                          onChange={this.onChangeNewUser}>
                          <option aria-selected>Seleccione Empresa</option>
                          {this.empresasList()}
                        </select>
                      </div>
                      <div className="container col-xs-12">
                        <label>Cliente</label>
                      </div>
                      <div className="container col-xs-12">
                        <div className="form-check">
                          <input className="form-check-input" type="radio" name="txtValidez" id="gridRadios1" value='S' onChange={this.onChangeRadio} />
                          <label className="form-check-label" htmlFor="gridRadios1">Activo</label>
                        </div>
                        <div className="form-check">
                          <input className="form-check-input" type="radio" name="txtValidez" id="gridRadios2" value='N' onChange={this.onChangeRadio} />
                          <label className="form-check-label" htmlFor="gridRadios2">Inactivo</label>
                        </div>
                        <div className="row col-xs-12">
                          <label className="col-md-12" htmlFor="claveUs">Clave</label>
                        </div>
                        <div className="row col-xs-12">
                          <input className="col-md-8" name="txtClave" value={this.state.txtClave} id="claveUs" type="password" placeholder="clave acceso" onChange={this.onChangeNewUser} />
                        </div>
                      </div>
                    </div>
                  </form>
                </div>
                <div className="modal-footer bg-dark">
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.grabaUsuarios}>Guardar</button>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                {this.encabezaList()}
              </thead>
              <tbody className="grid">
                {this.usersList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}
