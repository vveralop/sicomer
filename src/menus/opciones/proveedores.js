import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export class proveedores extends Component {
  constructor(props) {
    super()

    this.state = {
      selcomuna: [],
      provee: [],
      txtIdusuarioTit: '',
      txtIdempresa: '',
      txtNombreusuarioTit: '',
      txtToken: '',
      IngrModifi: '',

      txtIdproveedor: '',
      txtNombreproveedor: '',
      txtContacto: '',
      txtNombreContacto: '',
      txtRegion: '',
      txtComuna: ''
    }

    this.componentDidMount = this.componentDidMount.bind(this)
  }

  componentDidMount = (e) => {
    document.getElementById("borrar").className = "col-xl-2 invisible"
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;

    this.setState({ txtIdempresa: empresa });
    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });

    this.lecturaInicial(empresa, acceso, usuario);
  }

  lecturaInicial = async (empre, tok, user) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/proveedores'
    };

    const tokenDatos = this.codificaToken({
      idempresa: empre,
      idusuario: user,
      token: tok
    });

    const res = await axios.get(urlLogin + '/menu/proveedores?Z=' + tokenDatos, headers);
    this.setState({ provee: res.data })
  }

  onClickSelecctionComuna = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/proveedores/comunas'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtIdempresa,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/proveedores/comunas?Z=' + tokenDatos, headers)
    this.setState({ selcomuna: res.data })
  }

  proveeList = () => {
    return (
      this.state.provee.map((prov) => (
        <option key={prov.idproveedor} value={prov.nombreproveedor}>
          [Rut: {prov.idproveedor} ] {prov.nombreproveedor}
        </option>)
      )
    )
  }

  onClickProveedor = () => {
    this.lecturaInicial(this.state.txtIdempresa, this.state.txtToken, this.state.txtIdusuarioTit);
    this.onClickSelecctionComuna();
    this.labelModal('Crear Datos De Proveedores');
    this.setState ({ IngrModifi: 'I' });
  }

  OnClickClose = async (e) => {
    const res = await axios.post(urlLogin + '/salir', { idusuario: this.state.txtIdusuario });
    if (res.status === 200) {
      alert('Registro update Correctamente.')
      this.limpiaDatos();
    } else {
      alert('Ha habido un error al salir del sistema. Avise al Administrador.')
    }
  }

  proveedoresList = (e) => {
    return (this.state.provee.map((prov, index) => (
      <tr key={prov.idproveedor}>
        <td className="active small" maxLength="15">{prov.idproveedor}</td>
        <td className="active small" maxLength="50">{prov.nombreproveedor}</td>
        <td className="active small" maxLength="50">{prov.contacto}</td>
        <td className="active small" maxLength="50">{prov.nombrecontacto}</td>
        <td className="active small" maxLength="40">{prov.nombrecomuna}</td>
        <td className="active small" maxLength="40">{prov.nombreregion}</td>
        <td>
          <button type="submit"
            name="botonModal"
            className="btn btn-danger"
            data-toggle="modal"
            data-target="#exampleModal"
            value={index}
            data-whatever="vm" onClick={this.onClickModal}></button>
        </td>
      </tr>)))
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  grabaProveedores = async () => {
    if (this.state.txtIdproveedor === '' ||
      this.state.txtNombreproveedor === '' ||
      this.state.txtContacto === '' ||
      this.state.txtNombreContacto === '' ||
      this.state.txtComuna === '') {
      alert('Todos los campos deben ser completados.')
    } else {
      const tokenDatos = this.codificaToken({
        idusuario: this.state.txtIdusuarioTit,
        idempresa: this.state.txtIdempresa,
        idproveedor: this.state.txtIdproveedor,
        nombreprov: this.state.txtNombreproveedor,
        contacto: this.state.txtContacto,
        nomcontacto: this.state.txtNombreContacto,
        idcomuna: this.state.txtComuna,
        tipograba: this.state.IngrModifi,
        token: this.state.txtToken
      });

      const res = await axios.post(urlLogin + '/menu/proveedores', { body: tokenDatos });
      if (res.status === 200) {
        alert('Registro Insertado Correctamente.')
        this.limpiaDatos();
        this.lecturaInicial(this.state.txtIdempresa, this.state.txtToken);
      } else {
        alert(res.data['Registro']);
      }
    }
  }

  onClickModal = (e) => {
    const registro = e.target.value;
    const idproveedor = this.state.provee[registro].idproveedor;
    const nombreprove = this.state.provee[registro].nombreproveedor;
    const contacto = this.state.provee[registro].contacto;
    const nombreconta = this.state.provee[registro].nombrecontacto;
    const idcomuna = this.state.provee[registro].idcomuna;

    this.onClickSelecctionComuna();

    this.setState({
      txtIdproveedor: idproveedor,
      txtNombreproveedor: nombreprove,
      txtContacto: contacto,
      txtNombreContacto: nombreconta,
      txtComuna: idcomuna
    })
    this.labelModal('Modifica Datos de Proveedores');
    this.setState ({ IngrModifi: 'U' });
    document.getElementById('idprovee').setAttribute('readOnly', true);
  }

  cierraModal = (e) => {
    this.limpiaDatos();
    document.getElementById("idprovee").className = "col-md-4"
    document.getElementById("nombreprovee").className = "col-md-8"
    document.getElementById('idprovee').removeAttribute('readOnly');
  }

  escFunction = (e) => {
    if (e.keyCode === 27) {
      this.limpiaDatos();
    }
  }

  // validaToken = async (tok) => {
  //   const headers = {
  //     'Content-Type': 'application/json',
  //     'Access-Control-Allow-Origin': 'http://localhost:5000/menu/users'
  //   };

  //   const res = await axios.get(urlLogin + '/menu/users/valida?valtok=' + tok, headers);
  //   return (res.data)
  // }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  labelModal = (texto) => {
    document.getElementById("labelcremod").innerHTML = texto;
  }

  comunasList = () => {
    if (this.state.selcomuna === null) {
      return (null)
    } else {
    return (
      this.state.selcomuna.map((comu) => (
        <option key={comu.idcomuna} value={comu.idcomuna}>
          [Id: {comu.idcomuna} ] {comu.nombrecomuna}
        </option>)
      )
    )}
  }

  limpiaDatos = () => {
    this.setState({
      txtIdproveedor: '',
      txtNombreproveedor: '',
      txtContacto: '',
      txtNombreContacto: '',
      txtComuna: ''
    })
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Proveedores
                <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
            <div className="form-inline">
              <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClickProveedor}>Agregar Proveedor</button>
            </div>
          </div>
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content col-xs-12">
                <div className="modal-header bg-dark text-white">
                <h5 className="modal-title" id="labelcremod">Modifica Datos de Proveedores</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="container">
                  <form className="form-group">
                    <div className="container col-xs-12">
                      <label className="col-xl-4" id="labelprov" htmlFor="idprovee">Id</label>
                      <label className="col-xl-8" htmlFor="nombreprovee">Nombre Proveedor</label>
                    </div>
                    <div className="row col-md-12">
                      <input name="txtIdproveedor" maxLength="15" value={this.state.txtIdproveedor} id="idprovee" type="text" placeholder="contacto" className="col-md-4" onChange={this.onChangeNewUser} />
                      <input name="txtNombreproveedor" maxLength="50" value={this.state.txtNombreproveedor} id="nombreprovee" type="text" placeholder="Nombre contacto" className="col-md-8" onChange={this.onChangeNewUser} />
                    </div>
                    <div className="container col-xs-12">
                      <label className="col-xl-4" id="labelprov" htmlFor="contacto">Contacto</label>
                      <label className="col-xl-8" htmlFor="nombrecontacto">Nombre Contacto</label>
                    </div>
                    <div className="row col-md-12">
                      <input name="txtContacto" maxLength="50" value={this.state.txtContacto} id="contacto" type="text" placeholder="contacto" className="col-md-4" onChange={this.onChangeNewUser} />
                      <input name="txtNombreContacto" maxLength="50" value={this.state.txtNombreContacto} id="nombrecontacto" type="text" placeholder="Nombre contacto" className="col-md-8" onChange={this.onChangeNewUser} />
                    </div>
                    <div className="container col-xs-10">
                      <label className="container col-xl-12" htmlFor="comuna">Comuna</label>
                    </div>
                    <div className="row col-xl-10">
                      <select name="txtComuna" id="comuna"
                        className="selectpicker form-control col-md-12"
                        value={this.state.txtComuna}
                        onChange={this.onChangeNewUser}>
                        <option aria-selected>Seleccione Comuna de la lista</option>
                        {this.comunasList()}
                      </select>
                    </div>
                  </form>
                </div>
                <div className="modal-footer bg-dark col-xs-10">
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.grabaProveedores}>Guardar</button>
                  <button id="borrar" type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" >Borrar</button>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                <tr>
                  <th scope="col" className="small font-weight-bold">Id</th>
                  <th scope="col" className="small font-weight-bold">Nombre Proveedor</th>
                  <th scope="col" className="small font-weight-bold">Contacto</th>
                  <th scope="col" className="small font-weight-bold">Nombre</th>
                  <th scope="col" className="small font-weight-bold">Comuna</th>
                  <th scope="col" className="small font-weight-bold">Región</th>
                  <th scope="col" className="small font-weight-bold">Acción</th>
                </tr>
              </thead>
              <tbody className="grid">
                {this.proveedoresList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}

export default proveedores