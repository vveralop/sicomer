import React, { Component } from 'react'
import { BrowserRouter as Router } from 'react-router-dom';
import axios from 'axios';
import base64url from 'base64url';
import CryptoJS from 'crypto-js';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class modalcamclave extends Component {
    constructor(props) {
        super()
        this.state = {
            txtidUsuario: '',
            txtclaveAnt: '',
            txtclaveNueva: '',
            txtClave: ''
        }
    }

    componentDidMount = (e) => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombreempresa } = this.props.location.state;
        this.setState({ txtNombreEmpresa: nombreempresa });
        this.setState({ txtClave: acceso });
        this.setState({ txtidUsuario: usuario });
        document.getElementById("modalclave").className = "container";
        document.getElementById('user').setAttribute('readOnly', true);
        document.getElementById('pwdant').focus();
    }

    handleChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    saleModal = () => {
        document.getElementById("modalclave").className = "invisible";
    }

    onClickGrabar = async () => {

        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/cambiaclave',
            'mode': 'no-cors'
        };
        const ttoClaveant = CryptoJS.HmacSHA256(this.state.txtclaveAnt, secret).toString();
        const ttoClavenew = CryptoJS.HmacSHA256(this.state.txtclaveNueva, secret).toString();

        const tokenDatos = this.codificaToken({
            idusuario: this.state.txtidUsuario,
            claveant: ttoClaveant,
            clavenueva: ttoClavenew,
            token: this.state.txtClave
        });

        const res = await axios.put(urlLogin + '/menu/cambiaclave?Z=' + tokenDatos, headers)
        if (res.status === 200) {
            alert('Registro Actualizado Correctamente.')
            this.limpiaDatos();
            this.saleModal();
        } else {
            alert('Error: ' + res.data['conected']);
        }
    }

    limpiaDatos = () => {
        this.setState({
            txtclaveAnt: '',
            txtclaveNueva: ''
        });
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));

        return tokenRet;
    }

    render() {
        return (
            <Router>
                <div id="modalclave" className="container">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header navbar-dark bg-dark text-light">
                                <h5 className="modal-title" id="exampleModalLabel">Cambio de Clave Usuario</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.saleModal}>
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <label htmlFor="basic-url">Usuario</label>
                                <div className="input-group input-group-sm mb-3">
                                    <input id="user" type="text" name="txtUsuario" className="form-control" placeholder="Usuario" required value={this.state.txtidUsuario} />
                                </div>
                                <label htmlFor="basic-url">Clave Anterior</label>
                                <div className="input-group input-group-sm mb-3">
                                    <input id="pwdant" type="password" name="txtclaveAnt" className="form-control" placeholder="Password anterior" required value={this.state.txtclaveAnt} onChange={this.handleChange} />
                                </div>
                                <label htmlFor="basic-url">Clave Nueva</label>
                                <div className="input-group input-group-sm mb-3">
                                    <input id="pwdnew" type="password" name="txtclaveNueva" className="form-control" placeholder="Password nueva" required value={this.state.txtclaveNueva} onChange={this.handleChange} />
                                </div>
                            </div>
                            <div className="modal-footer bg-dark col-xs-10">
                                <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.onClickGrabar}>Grabar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}
