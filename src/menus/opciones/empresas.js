import React, { Component } from 'react'
import axios from 'axios';
import { BrowserRouter as Router } from 'react-router-dom';
import jwt from 'jsonwebtoken';
import base64url from 'base64url';

import { data, calculo, urlLogin } from '../../configuraciones/data';
const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class Empresas extends Component {
    constructor(props) {
        super()

        this.state = {
            empresas: [],
            sistemas: [],
            txtIdusuario: '',
            txtNombreusuario: '',
            txtNombreEmpTit: '',
            txtToken: '',

            txtTelefono: '',
            txtidEmpresa: '',
            txtNombrempre: '',
            txttwitterEmpresa: '',
            txtfaceEmpresa: '',
            txtinstaEmpresa: '',
            txtsistemaEmpresa: '',
            txtfechaVigencia: '',

            txtfecsistema: '',
            txtcajainicial: 0,
            txtiniciodia: 1,
            txtcierredia: 0,
            txtfecposterior: '',
            txtfecanterior: '',
            txtimagen: '',
            txtuserempre: 'postgres',
            txtNombreEmpresa: ''
        }

        this.onChangeNewEmpresa = this.onChangeNewEmpresa.bind(this);
        this.cierraModal = this.cierraModal.bind(this);
        this.limpiaDatos = this.limpiaDatos.bind(this);
        this.escFunction = this.escFunction.bind(this);
        this.grabaEmpresa = this.grabaEmpresa.bind(this);
        this.onClickCreaEmpresa = this.onClickCreaEmpresa.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount = async (e) => {
        document.getElementById("btnBorrar").className = "col-xl-2 invisible"
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuario: usuario });
        this.setState({ txtNombreusuario: nombre });
        this.setState({ txtNombreEmpTit: empresa });
        this.lecturaInicial(empresa);
    }

    lecturaInicial = async (empre, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/empresas',
            'mode': 'no-cors'
        };

        const tokenDatos = this.codificaToken({
            idemp: empre,
            idusuario: user,
            token: this.state.txtToken
        });

        const res = await axios.get(urlLogin + '/menu/empresas?Z=' + tokenDatos, headers)
        this.setState({ empresas: res.data })
        this.onClickSelecction();
    }

    cierraModal = (e) => {
        this.limpiaDatos();
        document.getElementById("btnBorrar").className = "col-xl-2 invisible"
    }

    escFunction = (e) => {
        if (e.keyCode === 27) {
            this.limpiaDatos();
            document.getElementById("btnBorrar").className = "col-xl-2 invisible"
        }
    }

    onClickCreaEmpresa = (e) => {
        this.limpiaDatos();
        this.labelModal('Crear Datos de Empresa');
    }

    limpiaDatos = () => {
        this.setState({
            txtidEmpresa: '',
            txtNombrempre: '',
            txttwitterEmpresa: '',
            txtfaceEmpresa: '',
            txtinstaEmpresa: '',
            txtsistemaEmpresa: '',
            txtfechaVigencia: '',
            txtTelefono: '',
            txtfecsistema: '',
            txtcajainicial: 0,
            txtiniciodia: 1,
            txtcierredia: 0,
            txtfecposterior: '',
            txtfecanterior: '',
            txtimagen: '',
            txtuserempre: 'postgres'
        })
        document.getElementById("idempresa").readOnly = false;
    }

    borrarEmpresa = async (e) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/empresas'
//            'Authorization': this.state.txtToken
        };

        const tokenDatos = this.codificaToken({
            idusuario: this.state.txtIdusuario,
            idemp: this.state.txtidEmpresa,
            token: this.state.txtToken
        });

        const res = await axios.delete(urlLogin + '/menu/empresas?Z=' + tokenDatos, headers)
        if (res.status === 200) {
            alert('Registro Borrado Correctamente.')
            this.limpiaDatos();
            this.lecturaInicial(this.state.txtidEmpresa, this.state.txtIdusuario);
        } else {
            alert('Ha habido un error al eliminar el registro. Avise al Administrador.');
        }
    }

    grabaEmpresa = async (e) => {
        //valida que no falte ningún campo
        if (this.state.txtidEmpresa === '' || this.state.txtNombrempre === '' || this.state.txtTelefono === '' ||
            this.state.txttwitterEmpresa === '' || this.state.txtfaceEmpresa === '' || this.state.txtinstaEmpresa === '' ||
            this.state.txtsistemaEmpresa === '' || this.state.txtfechaVigencia === '' ||
            this.state.txtfecsistema === '' || this.state.txtiniciodia === '' ||
            this.state.txtcierredia === '' || this.state.txtfecanterior === '' ||
            this.state.txtfecposterior === '' ) {
            alert('Todos los campos deben ser completados.')
        } else {
            const tokenDatos = this.codificaToken({
                idusuario: this.state.txtIdusuario,
                idemp: this.state.txtidEmpresa,
                nomemp: this.state.txtNombrempre,
                fonoemp: this.state.txtTelefono,
                twitemp: this.state.txttwitterEmpresa,
                faceemp: this.state.txtfaceEmpresa,
                instemp: this.state.txtinstaEmpresa,
                sistemp: this.state.txtsistemaEmpresa,
                fecvig: this.state.txtfechaVigencia,
                token: this.state.txtToken,
                fecsis: this.state.txtfecsistema,
                cajadia: this.state.txtcajainicial,
                inidia: this.state.txtiniciodia,
                findia: this.state.txtcierredia,
                fecpos: this.state.txtfecposterior,
                fecant: this.state.txtfecanterior,
                imagene: this.state.txtimagen,
                userempre: this.state.txtuserempre
            });
            const res = await axios.post(urlLogin + '/menu/empresas', { body: tokenDatos });
            if (res.status === 200) {
                alert('Registro Grabado Correctamente.')
                this.limpiaDatos();
                this.lecturaInicial(this.state.txtidEmpresa, this.state.txtIdusuario);
                document.getElementById("btnBorrar").className = "col-xl-2 invisible"
                document.getElementById("idempresa").readOnly = false;
            } else {
                alert('Error al grabar: ' + res.data['conected']);
            }
        }
    }

    sistemasList = () => {
        return (
            this.state.sistemas.map((siste) => (
                <option key={siste.idsistema} value={siste.idsistema}>
                    [ ID: {siste.idsistema} ] {siste.nombresistema}
                </option>)))
    }

    empresasList = (props) => {
        if (this.state.empresas !== null) {
            return (this.state.empresas.map((empr, index) => (
                <tr key={empr.idEmpresa}>
                    <td className="active small">{empr.idEmpresa}</td>
                    <td className="active small">{empr.nombreEmpresa}</td>
                    <td className="active small">{empr.telefonoEmpresa}</td>
                    <td className="active small">{empr.twitterEmpresa}</td>
                    <td className="active small">{empr.faceEmpresa}</td>
                    <td className="active small">{empr.instaEmpresa}</td>
                    <td className="active small">{empr.sistemaEmpresa}</td>
                    <td className="active small">{empr.usuarioCrea}</td>
                    <td className="active small">{empr.fechacrea}</td>
                    <td className="active small">{empr.fechaVigencia}</td>
                    <td>
                        <button type="submit"
                            name="botonModal"
                            className="btn btn-danger"
                            data-toggle="modal"
                            data-target="#exampleModal"
                            value={index}
                            data-whatever="vm" onClick={this.onClickModal}></button>
                    </td>
                </tr>)))
        } else {
            return (<tr><td className="small" maxLength="10">Sin datos para mostrar</td></tr>)
        }
    }

    onClickModal = (e) => {
        document.getElementById("btnBorrar").className = "btn btn-outline-success btn-sm my-2 my-sm-0"
        document.getElementById("idempresa").readOnly = true;
        const registro = e.target.value;
        const idempresa = this.state.empresas[registro].idEmpresa;
        const nombreemp = this.state.empresas[registro].nombreEmpresa;
        const teleempre = this.state.empresas[registro].telefonoEmpresa;
        const twiteremp = this.state.empresas[registro].twitterEmpresa;
        const faceempre = this.state.empresas[registro].faceEmpresa;
        const instempre = this.state.empresas[registro].instaEmpresa;
        const fechaempr = this.state.empresas[registro].fechaVigencia;
        const sistempre = this.state.empresas[registro].sistemaEmpresa;
        const fechacrea = this.state.empresas[registro].fechacrea;
        const usuarcrea = this.state.empresas[registro].usuarioCrea;
        const fecistema = this.state.empresas[registro].fechasistema;
        const cajainici = this.state.empresas[registro].cajainicialdia;
        const iniciodia = this.state.empresas[registro].iniciodia;
        const findedia  = this.state.empresas[registro].findedia;
        const fechaante = this.state.empresas[registro].fechaanterior;
        const fechapost = this.state.empresas[registro].fechaposterior;
        const imageempr = this.state.empresas[registro].imagen;
        const userempre = this.state.empresas[registro].userempresa;

        this.onClickSelecction();

        this.setState({
            txtidEmpresa: idempresa,
            txtNombrempre: nombreemp,
            txtTelefono: teleempre,
            txttwitterEmpresa: twiteremp,
            txtfaceEmpresa: faceempre,
            txtinstaEmpresa: instempre,
            txtsistemaEmpresa: sistempre,
            txtfechaVigencia: fechaempr,
            txtfechaCrea: fechacrea,
            txtusuarioCrea: usuarcrea,
            txtfecsistema: fecistema,
            txtcajainicial: cajainici,
            txtiniciodia: iniciodia,
            txtcierredia: findedia,
            txtfecposterior: fechapost,
            txtfecanterior: fechaante,
            txtimagen: imageempr,
            txtuserempre: userempre
        })

        this.labelModal('Modifica Datos de Empresas');
    }

    labelModal = (texto) => {
        document.getElementById("labelcremod").innerHTML = texto;
    }

    onClickSelecction = async () => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/empresas/select'
        };
        const tokenDatos = this.codificaToken({
            idemp: this.state.txtNombreEmpTit,
            idusuario: this.state.txtIdusuario,
            token: this.state.txtToken
        });

        const res = await axios.get(urlLogin + '/menu/empresas/select?Z=' + tokenDatos, headers)
        if (res.status === 200) {
            this.setState({ sistemas: res.data })
        } else {
            alert('Error al traer sistemas: ' + res.data['conected']);
        }
    }

    onChangeNewEmpresa = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    // validaToken = async (tok) => {
    //     const headers = {
    //         'Content-Type': 'application/json',
    //         'Access-Control-Allow-Origin': 'http://localhost:5000/valida'
    //     };

    //     const res = await axios.get(urlLogin + '/valida?valtok=' + tok, headers);
    //     return (res.data)
    // }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));

        return tokenRet;
    }

    render() {
        return (
            <Router>
                <div>
                    <div className="navbar navbar-dark bg-dark text-light">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Empresas
                        <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuario})</p></h6>
                        <div className="form-inline">
                            <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClickCreaEmpresa}>Crear Empresa</button>
                        </div>
                    </div>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
                        <div className="modal-dialog modal-lg" role="document">
                            <div className="modal-content">
                                <div className="modal-header bg-dark text-white">
                                    <h5 className="modal-title" id="labelcremod">Crear/Modificar Datos de Empresas</h5>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="container">
                                    <form className="form-group">
                                        <div className="container">
                                            <div className="row col-xs-12">
                                                <label className="col-md-3" htmlFor="idempresa">Id</label>
                                                <label className="col-md-9" htmlFor="nombreempresa">Nombre Empresa</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <input name="txtidEmpresa" maxLength="15" value={this.state.txtidEmpresa} id="idempresa" type="text" placeholder="id empresa" className="col-md-3" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtNombrempre" maxLength="50" value={this.state.txtNombrempre} id="nombreempresa" type="text" placeholder="Nombre" className="col-md-9" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                            <div className="row col-xs-12">
                                                <label className="col-md-6" htmlFor="fonoempresa">Teléfono</label>
                                                <label className="col-md-6" htmlFor="twitter">Twitter Empresa</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <input name="txtTelefono" maxLength="15" value={this.state.txtTelefono} id="fonoempresa" type="text" placeholder="fono" className="col-md-6" onChange={this.onChangeNewEmpresa} />
                                                <input name="txttwitterEmpresa" maxLength="20" value={this.state.txttwitterEmpresa} id="twitter" type="text" placeholder="twitter" className="col-md-6" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                            <div className="row col-xs-12">
                                                <label className="col-md-5" htmlFor="correo">Instagram Empresa</label>
                                                <label className="col-md-7" htmlFor="faceemp">Facebook Empresa</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <input name="txtinstaEmpresa" maxLength="30" value={this.state.txtinstaEmpresa} id="correo" type="text" placeholder="insta" className="col-md-5" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtfaceEmpresa" maxLength="50" value={this.state.txtfaceEmpresa} id="faceemp" type="text" placeholder="face" className="col-md-7" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                            <div className="row col-xs-12">
                                                <label className="col-md-9" htmlFor="sistEmp">Sistema</label>
                                                <label className="col-md-3" htmlFor="faceemp">Figente Hasta</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <select name="txtsistemaEmpresa" id="sistEmp"
                                                    className="selectpicker form-control col-md-9"
                                                    value={this.state.txtsistemaEmpresa}
                                                    onChange={this.onChangeNewEmpresa}>
                                                    <option aria-selected >Seleccione Sistema</option>
                                                    {this.sistemasList()}
                                                </select>
                                                <input name="txtfechaVigencia" value={this.state.txtfechaVigencia} id="claveUs" type="text" placeholder="vigencia" className="col-md-3" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                            <div className="row col-xs-12">
                                                <label className="col-md-2" htmlFor="inidia">Inicio Día</label>
                                                <label className="col-md-2" htmlFor="findia">Fin día</label>
                                                <label className="col-md-2" htmlFor="cajini">Caja Inicial</label>
                                                <label className="col-md-6" htmlFor="imagen">Imagen</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <input name="txtiniciodia" maxLength="1" value={this.state.txtiniciodia} id="inidia" type="text" placeholder="inicio día" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtcierredia" maxLength="1" value={this.state.txtcierredia} id="findia" type="text" placeholder="fin día" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtcajainicial" maxLength="15" value={this.state.txtcajainicial} id="cajini" type="text" placeholder="caja inicial" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtimagen" maxLength="30" value={this.state.txtimagen} id="imagen" type="text" placeholder="nombre logotipo" className="col-md-6" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                            <div className="row col-xs-12">
                                                <label className="col-md-2" htmlFor="fecsis">F. Sistema</label>
                                                <label className="col-md-2" htmlFor="fecpos">F. Posterior</label>
                                                <label className="col-md-2" htmlFor="fecant">F. Anterior</label>
                                                <label className="col-md-6" htmlFor="usersis">User</label>
                                            </div>
                                            <div className="row col-xs-12">
                                                <input name="txtfecsistema" maxLength="10" value={this.state.txtfecsistema} id="fecsis" type="text" placeholder="fecha sistema" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtfecposterior" maxLength="10" value={this.state.txtfecposterior} id="fecpos" type="text" placeholder="fecha mañana" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtfecanterior" maxLength="10" value={this.state.txtfecanterior} id="fecant" type="text" placeholder="fecha anterior" className="col-md-2" onChange={this.onChangeNewEmpresa} />
                                                <input name="txtuserempre" maxLength="30" value={this.state.txtuserempre} id="usersis" type="text" placeholder="nombre logotipo" className="col-md-6" onChange={this.onChangeNewEmpresa} />
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="modal-footer bg-dark">
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.grabaEmpresa}>Guardar</button>
                                    <button type="button" id="btnBorrar" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.borrarEmpresa}>Borrar</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="table-responsive-sm">
                        <table className="table table-striped table-sm">
                            <thead className="thead-dark table-sm">
                                <tr>
                                    <th scope="col" className="small font-weight-bold">Id</th>
                                    <th scope="col" className="small font-weight-bold">Nombre Empresa</th>
                                    <th scope="col" className="small font-weight-bold">Teléfono</th>
                                    <th scope="col" className="small font-weight-bold">Twitter</th>
                                    <th scope="col" className="small font-weight-bold">Facebook</th>
                                    <th scope="col" className="small font-weight-bold">Instagram</th>
                                    <th scope="col" className="small font-weight-bold">Sis</th>
                                    <th scope="col" className="small font-weight-bold">Usuario Crea</th>
                                    <th scope="col" className="small font-weight-bold">Fecha Crea</th>
                                    <th scope="col" className="small font-weight-bold">Vigencia</th>
                                    <th scope="col" className="small font-weight-bold">Acción</th>
                                </tr>
                            </thead>
                            <tbody className="grid">
                                {this.empresasList()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </Router>
        )
    }
}