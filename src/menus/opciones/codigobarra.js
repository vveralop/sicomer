import React, { Component } from "react";
import { BrowserRouter as Router } from 'react-router-dom';
import { useBarcode } from "@createnextapp/react-barcode";

const BarCode = ({ textobarcode }) => {
    const { inputRef } = useBarcode({
        value: textobarcode === "" ? "vacio" : textobarcode,
        options: {
            background: "#ffff00",
            lineColor: "#1d1d1d",
            fontSize: 12,
            fontOptions: "bold"

        }
    });
    return <img className="border" alt="" ref={inputRef} />;
}

export default class Codigobarra extends Component {
    constructor(props) {
        super()

        this.state = {
            txtIdusuario: '',
            txtNombreusuario: '',
            txtNombreEmpTit: '',
            txtToken: '',
            txtcodigo: '',
            txtcodigo2: '',
            txtcodigo3: '',
            txtcodigo4: ''

        }
        this.componentDidMount = this.componentDidMount.bind(this);
        this.onChange = this.onChange.bind(this);
    }

    componentDidMount = async (e) => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuario: usuario });
        this.setState({ txtNombreusuario: nombre });
        this.setState({ txtNombreEmpTit: empresa });
    }

    onChange = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    render() {
        return (
            <Router>
                <div className="container-responsive-sm">
                    <div className="navbar navbar-dark bg-dark text-light mx-center">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto">Generación Códigos de Barras
                        <p className="text-center text-success small">
                                (Usuario: {this.state.txtNombreusuario})</p></h6>
                    </div>
                    <div className="container small row col-xl-12 my-0">
                        <label className="col-xl-3 border bg-dark text-light" htmlFor="codigo">Ingrese un Código</label>
                        <label className="col-xl-3 border bg-dark text-light" htmlFor="codigo2">Ingrese un Código</label>
                        <label className="col-xl-3 border bg-dark text-light" htmlFor="codigo3">Ingrese un Código</label>
                        <label className="col-xl-3 border bg-dark text-light" htmlFor="codigo4">Ingrese un Código</label>
                    </div>
                    <div className="container small row col-xl-12 my-0">
                        <input name="txtcodigo"  maxLength="15" value={this.state.txtcodigo}  id="codigo"  type="text" placeholder="ingrese codigo" className="col-xl-3" onChange={this.onChange} />
                        <input name="txtcodigo2" maxLength="15" value={this.state.txtcodigo2} id="codigo2" type="text" placeholder="ingrese codigo" className="col-xl-3" onChange={this.onChange} />
                        <input name="txtcodigo3" maxLength="15" value={this.state.txtcodigo3} id="codigo3" type="text" placeholder="ingrese codigo" className="col-xl-3" onChange={this.onChange} />
                        <input name="txtcodigo4" maxLength="15" value={this.state.txtcodigo4} id="codigo4" type="text" placeholder="ingrese codigo" className="col-xl-3" onChange={this.onChange} />
                    </div>
                </div>
                <div className="container-respponsive-sm border col-xl-5 py-2">
                    <BarCode textobarcode={this.state.txtcodigo} />
                    <BarCode textobarcode={this.state.txtcodigo2} />
                    <BarCode textobarcode={this.state.txtcodigo3} />
                    <BarCode textobarcode={this.state.txtcodigo4} />
                </div>
            </Router >
        )
    }
}