import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class iniciadia extends Component {
    constructor(props) {
        super()

        this.state = {
            fechas: [],
            txtIdusuarioTit: '',
            txtIdempresa: '',
            txtNombreusuarioTit: '',
            txtToken: '',

            txtfechaanterior: '',
            txtfechaposterior: '',
            txtfechahoy: '',
            txtiniciodia: '',
            txtfindia: '',
            txtcajadeldia: 0
        }

        this.componentDidMount = this.componentDidMount.bind(this);
        this.fechasList = this.fechasList.bind(this);

    }

    componentDidMount = () => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtIdempresa: empresa });
        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuarioTit: usuario });
        this.setState({ txtNombreusuarioTit: nombre });
        this.lecturaInicial(empresa, acceso, usuario);
    }

    lecturaInicial = async (empre, tok, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/inicio'
        };

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/inicio?Z=' + tokenDatos, headers)
        this.setState({ fechas: res.data })
        this.fechasList();
        this.validaFecha();
    }

    grabaFechas = async (e) => {
        if (this.state.txtfechaanterior === '' ||
            this.state.txtfechaposterior === '' ||
            this.state.txtfechahoy === '' ||
            this.state.txtcajadeldia === '') {
            alert('Todos los campos deben ser completados.')
        } else {
            const tokenDatos = this.codificaToken({
                idempresa: this.state.txtIdempresa,
                fecant: this.state.txtfechaanterior,
                fecpos: this.state.txtfechaposterior,
                fechoy: this.state.txtfechahoy,
                caja: this.state.txtcajadeldia,
                idusuario: this.state.txtIdusuarioTit,
                token: this.state.txtToken
            });

            const res = await axios.post(urlLogin + '/menu/inicio', { body: tokenDatos });
            if (res.status === 200) {
                alert('Inicio de día ejecutado Correctamente.')
                this.lecturaInicial(this.state.txtIdempresa, this.state.txtToken);
            } else {
                alert('Ha habido un error. Avise al Administrador. Status: ' + res['mensaje']);
            }
        }
    }

    validaFecha() {
        if (Number(this.state.txtiniciodia) === 1) {
            document.getElementById("span1").className = "text-success";
            document.getElementById("btnsmt").className = "btn-submit btn-success m-3 invisible";
        }
    }

    fechasList() {
        if (this.state.fechas !== null) {
            this.state.fechas.map((fec, index) => (
                this.setState({
                    txtfechaanterior: fec.fechaanterior,
                    txtfechaposterior: fec.fechaposterior,
                    txtfechahoy: fec.fechasistema,
                    txtiniciodia: fec.iniciodia,
                    txtfindia: fec.findedia,
                    txtcajadeldia: fec.cajainicialdia
                })
            ))
        }
    }

    onChangedata = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));

        return tokenRet;
    }

    render() {
        return (
            <Router>
                <div>
                    <div className="navbar navbar-dark bg-dark text-light">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Iniciar Día Comercial
                        <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
                    </div>
                    <div className="border bg-dark">
                        <div className="container text-white row mx-5">
                            <div className="container pl-5">
                                <label className="border col-2 text-center small m-3" htmlFor="fanterior">Fecha Anterior</label>
                                <input type="text" className="col-2 small text-center" value={this.state.txtfechaanterior} maxLength="10" id="fanterior" onChange={this.onChangedata} />
                            </div>
                            <div className="container pl-5">
                                <label className="border col-2 text-center small m-3" htmlFor="fhoy">Fecha Hoy</label>
                                <input type="text" className="col-2 small text-center" value={this.state.txtfechahoy} maxLength="10" id="fhoy" onChange={this.onChangedata} />
                            </div>
                            <div className="container pl-5">
                                <label className="border col-2 text-center small m-3" htmlFor="fprox">Fecha Siguiente</label>
                                <input type="text" className="col-2 small text-center" value={this.state.txtfechaposterior} maxLength="10" id="fprox" onChange={this.onChangedata} />
                            </div>
                            <div className="container pl-5">
                                <label className="border col-2 text-center small m-3" htmlFor="cinicial">Ingrese Caja Inicial</label>
                                <input type="text" className="col-2 small text-right text-center" value={this.state.txtcajadeldia} maxLength="15" id="cinicial" onChange={this.onChangedata} />
                            </div>
                            <div className="container pl-5">
                                <button id="btnsmt" type="submit" className="btn-submit btn-success m-3" onClick={this.grabaFechas}>Inicia Día</button>
                                <span className="text-success invisible" id="span1">Inicio de dia ya fue realizado.</span>
                            </div>
                        </div>
                    </div>
                </div>
            </Router>
        )
    }
}