import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import moment from 'moment';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');
const fechaformato = moment().format("DD-MM-YYYY");

export default class Productos extends Component {
  constructor(props) {
    super()

    this.state = {
      product: [],
      selprovee: [],
      tprod: [],
      txtIdusuarioTit: '',
      txtIdempresa: '',
      txtNombreusuarioTit: '',
      txtToken: '',

      txtIdproducto: '',
      txtNombreProducto: '',
      txtFechaCompra: '',
      txtIdproveedor: '',
      txtFechaVcto: '',
      txtTipoProducto: '',
      txtPrecioRef: '',
      txtNroFactura: '',
      txtunidadMedida: '',
      esInsert: ''
    }

    this.onClicknewTproducto = this.onClicknewTproducto.bind(this);
    this.onClickSelecctionProveedor = this.onClickSelecctionProveedor.bind(this);
    this.componentDidMount = this.componentDidMount.bind(this);
    this.onClickProducto = this.onClickProducto.bind(this);
  }

  componentDidMount = () => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;

    this.setState({ txtIdempresa: empresa });
    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.lecturaInicial(empresa, acceso, usuario);
    document.getElementById('idproducto').setAttribute('readOnly', false);
  }

  lecturaInicial = async (empre, tok, user) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/productos'
    };

    const tokenDatos = this.codificaToken({
      idempresa: empre,
      idusuario: user,
      token: tok
    });

    const res = await axios.get(urlLogin + '/menu/productos?Z=' + tokenDatos, headers)
    this.setState({ product: res.data })
  }

  onClickSelecctionProveedor = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/productos/selprov'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtIdempresa,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/productos/selprov?Z=' + tokenDatos, headers)
    this.setState({ selprovee: res.data })
  }

  onClicknewTproducto = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/productos/tprod'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtIdempresa,
      idusuario: this.state.txtIdusuarioTit,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/productos/tprod?Z=' + tokenDatos, headers)
    this.setState({ tprod: res.data })
  }

  onClickProducto() {
    document.getElementById('idproducto').removeAttribute("readOnly");
    this.onClicknewTproducto();
    this.onClickSelecctionProveedor();
    this.setState({
      txtIdproducto: '',
      txtFechaCompra: fechaformato,
      txtFechaVcto: fechaformato,
      esInsert: 'S'
    })
    this.labelModal('Crea Datos de Productos');
  }

  tprodList() {
    if (this.state.tprod !== null) {
      return (
        this.state.tprod.map((prod) => (
          <option key={prod.tipoproducto} value={prod.tipoproducto}>
            [Código: {prod.tipoproducto} ] {prod.descripcion}
          </option>)))
    }
  }

  labelModal = (texto) => {
    document.getElementById("labelcremod").innerHTML = texto;
  }

  proveedoresList() {
    if (this.state.selprovee === null) {
      return (null)
    } else {
      return (
        this.state.selprovee.map((prov) => (
          <option key={prov.idproveedor} value={prov.idproveedor}>
            [Código: {prov.idproveedor} ] {prov.nombreproveedor}
          </option>)))
    }
  }

  OnClickClose = async (e) => {
    const res = await axios.post(urlLogin + '/salir', { idusuario: this.state.txtIdusuario });
    if (res.status === 200) {
      alert('Registro update Correctamente.')
      this.limpiaDatos();
    } else {
      alert('Ha habido un error al salir del sistema. Avise al Administrador.')
    }
  }

  colorcondicion = (fecha) => {
    if (fecha >= fechaformato) {
      return ("active small text-danger");
    } else {
      return ("active small");
    }
  }

  productosList = (e) => {
    if (this.state.product !== null) {
      return (this.state.product.map((prod, index) => (
        <tr key={prod.idproducto}>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="10">{prod.idproducto}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="50">{prod.descripcion}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="50">{prod.nombreproducto}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="10">{prod.fechacompra}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="50">{prod.nombreproveedor}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="10">{prod.fechavencimiento}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="10">{prod.precioref}</td>
          <td className={this.colorcondicion(prod.fechavencimiento)} maxLength="10">{prod.unidadmedida}</td>
          <td>
            <button type="submit"
              name="botonModal"
              className="btn btn-danger"
              data-toggle="modal"
              data-target="#exampleModal"
              value={index}
              data-whatever="vm" onClick={this.onClickModal}></button>
          </td>
        </tr>)))
    }
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  borraProductos = async (e) => {

    if (this.state.txtIdproducto === this.revisaSiexisteProd()) {
      const headers = {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': 'http://localhost:5000/menu/productos'
      };

      const tokenDatos = this.codificaToken({
        idusuario: this.state.txtIdusuarioTit,
        idempresa: this.state.txtIdempresa,
        idproducto: this.state.txtIdproducto,
        token: this.state.txtToken
      });

      const res = await axios.delete(urlLogin + '/menu/productos?Z=' + tokenDatos, headers);
      if (res.status === 200) {
        console.log(res);
        alert('Registro Borrado Correctamente.')
        this.limpiaDatos();
        this.lecturaInicial(this.state.txtIdempresa, this.state.txtToken);
      } else {
        alert('Ha habido un error. Avise al Administrador. Status: ' + res.data['registro']);
      }
    }
  }

  grabaProductos = async (e) => {
    var graba = 'S';
    //valida que no falte ningún campo
    if (this.state.txtNombreProducto === '' ||
      this.state.txtFechaCompra === '' ||
      this.state.txtIdproveedor === '' ||
      this.state.txtFechaVcto === '' ||
      this.state.txtTipoProducto === '' ||
      this.state.txtPrecioRef === '' ||
      this.state.txtunidadMedida === '') {
      alert('Todos los campos deben ser completados.')
    } else {
      if (this.state.esInsert === 'S') {
        const valida  = this.revisaSiexisteProd() ;
        if (valida==='Z'){
           graba = 'S';           
        } else {
           graba = 'N';
        }
      } else {
        graba = 'S';
      }
      if (graba==='S'){
        const tokenDatos = this.codificaToken({
          idusuario: this.state.txtIdusuarioTit,
          idempresa: this.state.txtIdempresa,
          idproducto: this.state.txtIdproducto,
          nombreProducto: this.state.txtNombreProducto,
          fechaCompra: this.state.txtFechaCompra,
          idProveedor: this.state.txtIdproveedor,
          fechaVcto: this.state.txtFechaVcto,
          tipoProducto: this.state.txtTipoProducto,
          precioRef: this.state.txtPrecioRef,
          unidadmed: this.state.txtunidadMedida,
          token: this.state.txtToken
        });

        const res = await axios.post(urlLogin + '/menu/productos', { body: tokenDatos });
        if (res.status === 200) {
          alert('Registro Insertado Correctamente.')
          console.log(res.data)
          this.limpiaDatos();
          this.lecturaInicial(this.state.txtIdempresa, this.state.txtToken);
        } else {
          alert('Ha habido un error. Avise al Administrador. Status: ' + res.data);
        }
      } else {
        alert('Producto con el código ingresado ya existe. Ingrese otro código.');
      }
    }
  }

  onClickModal = (e) => {
    document.getElementById('idproducto').setAttribute('readOnly', true);
    this.setState({ esInsert: 'N'});
    const registro = e.target.value;
    const idproducto = this.state.product[registro].idproducto;
    const nombreprodu = this.state.product[registro].nombreproducto;
    const fechacompr = this.state.product[registro].fechacompra;
    const idproveedo = this.state.product[registro].idproveedor;
    const fechavcto = this.state.product[registro].fechavencimiento;
    const tipoproduc = this.state.product[registro].tipoproducto;
    const preciorefe = this.state.product[registro].precioref;
    const unidadmeda = this.state.product[registro].unidadmedida;

    this.onClicknewTproducto();
    this.onClickSelecctionProveedor();

    this.setState({
      txtIdproducto: idproducto,
      txtNombreProducto: nombreprodu,
      txtFechaCompra: fechacompr,
      txtIdproveedor: idproveedo,
      txtFechaVcto: fechavcto,
      txtTipoProducto: tipoproduc,
      txtPrecioRef: preciorefe,
      txtunidadMedida: unidadmeda
    })

    this.labelModal('Modifica Datos de Productos');
  }

  cierraModal = (e) => {
    this.limpiaDatos();
    document.getElementById("idproducto").className = "col-md-2"
    document.getElementById("labelprod").className = "col-md-2"
    document.getElementById('idproducto').removeAttribute("readOnly");
  }

  escFunction = (e) => {
    if (e.keyCode === 27) {
      this.limpiaDatos();
    }
  }

  limpiaDatos = () => {
    this.setState({
      txtNombreProducto: '',
      txtFechaCompra: '',
      txtIdproveedor: '',
      txtFechaVcto: '',
      txtTipoProducto: '',
      txtPrecioRef: '',
      txtunidadMedida: ''
    })
  }

  revisaSiexisteProd() {
    if (this.state.product !== null) {
      const prod = this.state.product.find(prod => prod.idproducto === this.state.txtIdproducto);
      if (prod !== undefined) {
        return (prod.idproducto);
      } else {
        return ('Z');
      }
    }
  }

  // validaToken = async (tok) => {
  //   const headers = {
  //     'Content-Type': 'application/json',
  //     'Access-Control-Allow-Origin': 'http://localhost:5000/menu/users'
  //   };

  //   const res = await axios.get(urlLogin + '/menu/users/valida?valtok=' + tok, headers);
  //   return (res.data)
  // }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto">Mantenimiento de Productos
            <p className="text-center text-light small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
            <div className="form-inline">
              <button type="button" className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" onClick={this.onClickProducto}>Agregar Producto</button>
            </div>
          </div>
          <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
            <div className="modal-dialog modal-lg" role="document">
              <div className="modal-content col-xs-10">
                <div className="modal-header bg-dark text-white">
                  <h5 className="modal-title" id="labelcremod">Modificar Datos de Productos</h5>
                  <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                    <span aria-hidden="true">&times;</span>
                  </button>
                </div>
                <div className="container">
                  <form className="form-group">
                    <div className="container col-xs-12">
                      <label className="col-xl-5" id="labelprod" htmlFor="idproducto">Id Prod</label>
                      <label className="col-xl-7" htmlFor="nombreprod">Nombre de Producto</label>
                    </div>
                    <div className="row col-md-12">
                      <input name="txtIdproducto" maxLength="40" value={this.state.txtIdproducto} id="idproducto" type="text" placeholder="código" className="col-md-5" onChange={this.onChangeNewUser} />
                      <input name="txtNombreProducto" maxLength="50" value={this.state.txtNombreProducto} id="nombreprod" type="text" placeholder="Nombre producto" className="col-md-7" onChange={this.onChangeNewUser} />
                    </div>
                    <div className="container col-xs-12">
                      <label className="col-xl-8" htmlFor="idprovee">Proveedor</label>
                      <label className="col-xl-4" htmlFor="precio">Precio Venta</label>
                    </div>
                    <div className="row col-xl-12">
                      <select name="txtIdproveedor" id="idprovee"
                        className="selectpicker form-control col-md-8"
                        value={this.state.txtIdproveedor}
                        onChange={this.onChangeNewUser}>
                        <option aria-selected>Seleccione Proveedor</option>
                        {this.proveedoresList()}
                      </select>
                      <input name="txtPrecioRef" value={this.state.txtPrecioRef} id="precio" type="text" placeholder="precio ref" className="col-md-4" onChange={this.onChangeNewUser} />
                    </div>
                    <div className="container col-xs-12">
                      <label className="container col-xl-6" htmlFor="tproduct">Tipo Producto</label>
                      <label className="container col-xl-6" htmlFor="unidadmedida">Unidad de Venta</label>
                    </div>
                    <div className="row col-xl-12">
                      <select name="txtTipoProducto" id="tproduct"
                        className="selectpicker form-control col-md-6"
                        value={this.state.txtTipoProducto}
                        onChange={this.onChangeNewUser}>
                        <option aria-selected>Seleccione Tipo Producto</option>
                        {this.tprodList()}
                      </select>
                      <select name="txtunidadMedida" id="unidadmedida"
                        className="selectpicker form-control col-md-6"
                        value={this.state.txtunidadMedida}
                        onChange={this.onChangeNewUser}>
                        <option value="">Medida Venta</option>
                        <option value="Kg">Kilogramos</option>
                        <option value="Unidades" selected>Unidades</option>
                      </select>
                    </div>
                    <div className="container col-xs-10">
                      <label className="container col-xl-3" htmlFor="fcpra">Fecha Compra</label>
                      <label className="container col-xl-5" htmlFor="fvcto">Fecha Vcto Producto</label>
                    </div>
                    <div className="row col-xl-10">
                      <input name="txtFechaCompra" maxLength="10" value={this.state.txtFechaCompra} id="fcpra" type="text" placeholder="fecha Compra" className="col-md-4" onChange={this.onChangeNewUser} />
                      <input name="txtFechaVcto" maxLength="10" value={this.state.txtFechaVcto} id="fvcto" type="text" placeholder="fecha de vencimiento" className="col-md-4" onChange={this.onChangeNewUser} />
                    </div>
                  </form>
                </div>
                <div className="modal-footer bg-dark col-xs-10">
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.grabaProductos}>Guardar</button>
                  <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.borraProductos}>Borrar</button>
                </div>
              </div>
            </div>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                <tr>
                  <th scope="col" className="small font-weight-bold">Id</th>
                  <th scope="col" className="small font-weight-bold">Tipo Producto</th>
                  <th scope="col" className="small font-weight-bold">Nombre Producto</th>
                  <th scope="col" className="small font-weight-bold">Fecha Compra</th>
                  <th scope="col" className="small font-weight-bold">Proveedor</th>
                  <th scope="col" className="small font-weight-bold">Fecha Vcto</th>
                  <th scope="col" className="small font-weight-bold">Precio</th>
                  <th scope="col" className="small font-weight-bold">Medida Venta</th>
                  <th scope="col" className="small font-weight-bold">Acción</th>
                </tr>
              </thead>
              <tbody className="grid">
                {this.productosList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}