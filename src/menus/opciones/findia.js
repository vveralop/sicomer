import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class findia extends Component {
  constructor(props) {
    super()

    this.state = {
      product: [],
      txtIdusuarioTit: '',
      txtIdempresa: '',
      txtNombreusuarioTit: '',
      txtToken: '',

      txtfechaanterior: '',
      txtfechaposterior: '',
      txtfechahoy: '',
      txtcajadeldia: 0,
      txtNombreEmpresa: ''
    }
  }

  componentDidMount = () => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;
    const { nombreempresa } = this.props.location.state;
    this.setState({ txtNombreEmpresa: nombreempresa });
    this.setState({ txtIdempresa: empresa });
    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.lecturaInicial(empresa, acceso, usuario);
  }

  lecturaInicial = async (empre, tok, user) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/flujocaja'
    };

    const tokenDatos = this.codificaToken({
      idempresa: empre,
      idusuario: user,
      token: tok
    });

    const res = await axios.get(urlLogin + '/menu/flujocaja?Z=' + tokenDatos, headers)
    this.setState({ product: res.data })

    console.log(res.data)
  }

  productosList = (e) => {
    return (this.state.product.map((prod, index) => (
      <tr key={prod.idproducto}>
        <td className="active small" maxLength="10">{prod.idproducto}</td>
        <td className="active small" maxLength="50">{prod.descripcion}</td>
        <td className="active small" maxLength="50">{prod.nombreproducto}</td>
        <td className="active small" maxLength="10">{prod.fechacompra}</td>
        <td className="active small" maxLength="50">{prod.nombreproveedor}</td>
        <td className="active small" maxLength="10">{prod.fechavencimiento}</td>
        <td className="active small" maxLength="10">{prod.precioref}</td>
        <td className="active small" maxLength="10">{prod.unidadmedida}</td>
      </tr>)))
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Calculo Flujo de Caja
            <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
          </div>
          <div className="navbar border navbar-dark bg-dark text-light small">
            <label className="col col-auto align-self-start" htmlFor="cinicial">Ingrese Caja Inicial</label>
            <input className="row col-2 align-self-start text-right" defaultValue="0" maxLength="15" id="cinicial" />
            
            <button className="btn-submit btn-success">Re-Calcular</button>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                <tr>
                  <th scope="col" className="small font-weight-bold">Id</th>
                  <th scope="col" className="small font-weight-bold">Tipo Producto</th>
                  <th scope="col" className="small font-weight-bold">Nombre Producto</th>
                  <th scope="col" className="small font-weight-bold">Fecha Compra</th>
                  <th scope="col" className="small font-weight-bold">Proveedor</th>
                  <th scope="col" className="small font-weight-bold">Fecha Vcto</th>
                  <th scope="col" className="small font-weight-bold">Precio</th>
                  <th scope="col" className="small font-weight-bold">Medida Venta</th>
                </tr>
              </thead>
              <tbody className="grid">
                {this.productosList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}