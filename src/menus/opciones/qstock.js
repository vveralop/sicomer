import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { filteres, filteresF, tittlesC, options, priceFormatter } from '../defgrillasconsulta';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');
const columns = [{
    dataField: 'idproducto',
    text: 'Código Producto',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'left',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10%", margin: "auto" },
    headerAlign: 'center'
}, {
    dataField: 'nombreproducto',
    text: 'Nombre Producto',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'left',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "15%", margin: "auto" },
    headerAlign: 'center'
}, {
    dataField: 'cantidad',
    text: 'Total Stock',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'center',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "5px", height: "20px" },
    headerAlign: 'center'
}, {
    dataField: 'unidadmedida',
    text: 'Unidad Venta',
    filter: textFilter({ style: filteresF }),
    style: tittlesC,
    align: 'center',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", margin: "auto" },
    headerAlign: 'center'
}, {
    dataField: 'precioref',
    text: 'Precio Venta',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'center',
    formatter: priceFormatter,
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", height: "20px" },
    headerAlign: 'center'
}];

export class qstock extends Component {
    constructor(props) {
        super()
        this.state = {
            stock: [],
            txtIdusuarioTit: '',
            txtNombreEmpTit: '',
            txtNombreusuarioTit: '',
            txtToken: ''
        }
    }

    componentDidMount = (e) => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuarioTit: usuario });
        this.setState({ txtNombreusuarioTit: nombre });
        this.setState({ txtNombreEmpTit: empresa });
        this.traerOperaciones(empresa, acceso, usuario);
    }

    onChangeOpe = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    traerOperaciones = async (empre, tok, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/qstock'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/qstock?Z=' + tokenDatos, headers)
        this.setState({ stock: res.data })
    }

    stockList = (e) => {
        if (this.state.stock !== null) {
            return (this.state.stock.map((ope, index) => (
                <tr key={index}>
                    <td className="small" maxLength="10">{ope.idproducto}</td>
                    <td className="small" maxLength="10">{ope.nombreproducto}</td>
                    <td className="small text-right" maxLength="10">{ope.precioref}</td>
                    <td className="small text-right" maxLength="15">{this.numberFormat(ope.cantidad)}</td>
                    <td className="small" maxLength="10">{ope.unidadmedida}</td>
                </tr>)))
        } else {
            return (<tr><td className="small" maxLength="10">Sin stock para mostrar</td></tr>)
        }
    }

    muestraData = (e) => {
        if (this.state.stock !== null) {
            return (<BootstrapTable
                keyField='idproducto'
                data={this.state.stock}
                columns={columns}
                filter={filterFactory()}
                filterPosition="top"
                pagination={paginationFactory(options)} />)
        } else {
            return (
                <table>
                    <thead className="table thead-dark table-sm">
                        <tr>
                            <th scope="col" className="small font-weight-bold">Id Producto</th>
                            <th scope="col" className="small font-weight-bold">Nombre Producto</th>
                            <th scope="col" className="small font-weight-bold">Precio Referencia</th>
                            <th scope="col" className="small font-weight-bold">Cantidad</th>
                            <th scope="col" className="small font-weight-bold">Unidad</th>
                        </tr>
                    </thead>
                    <tbody className="grid">
                        <tr><td className="small" maxLength="10">Sin datos para mostrar</td></tr>
                    </tbody>
                </table>)
        }
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));
        return tokenRet;
    }

    numberFormat(num) {
        return (Intl.NumberFormat("de-DE").format(num));
    }

    render() {
        return (
            <Router>
                <div>
                    <div className="navbar navbar-dark bg-dark text-light">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Consulta de Stock de Productos
                         <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
                    </div>
                        {/* {this.muestraData} */}
                        <BootstrapTable
                                    keyField='idproducto'
                                    data={this.state.stock}
                                    columns={columns}
                                    filter={filterFactory()}
                                    filterPosition="top"
                                    pagination={paginationFactory(options)}
                                />
                </div>
            </Router>
        )
    }
}

export default qstock;
