import React, { Component } from 'react';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import BootstrapTable from 'react-bootstrap-table-next';
import filterFactory, { textFilter } from 'react-bootstrap-table2-filter';
import paginationFactory from 'react-bootstrap-table2-paginator';
import { filteres, filteresF, tittlesC, options, priceFormatter } from '../defgrillasconsulta';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

// Aquí ibas las descripciones

const columns = [{
    dataField: 'idproducto',
    text: 'Código Producto',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'left',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10%", margin: "auto" },
    headerAlign: 'center'
    }, {
    dataField: 'nombreproducto',
    text: 'Nombre',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'left',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "15%", margin: "auto" },
    headerAlign: 'center'
    }, {
    dataField: 'descripcion',
    text: 'Agrupación',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'left',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "5px", height: "20px" },
    headerAlign: 'center'
    }, {
    dataField: 'precioref',
    text: 'Precio Venta',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    align: 'center',
    formatter: priceFormatter,
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", height: "20px" },
    headerAlign: 'center'
    }, {
    dataField: 'unidadmedida',
    text: 'Unidad Venta',
    filter: textFilter({ style: filteresF }),
    style: tittlesC,
    align: 'center',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", margin: "auto" },
    headerAlign: 'center'
    }, {
    dataField: 'fechacompra',
    text: 'Fecha Compra',
    filter: textFilter({ style: filteresF }),
    style: tittlesC,
    align: 'center',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", margin: "auto" },
    headerAlign: 'center'
    }, {
    dataField: 'fechavencimiento',
    text: 'Fecha Vcto',
    filter: textFilter({ style: filteresF }),
    style: tittlesC,
    align: 'center',
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", margin: "auto" },
    headerAlign: 'center'
    }, {
    dataField: 'nombreproveedor',
    text: 'Proveedor',
    filter: textFilter({ style: filteres }),
    style: tittlesC,
    headerStyle: { backgroundColor: '#343a40', color: "white", fontSize: "12px", width: "10px", margin: "auto" },
    headerAlign: 'center'}];

export default class prodquery extends Component {
    constructor(props) {
        super()

        this.state = {
            product: [],
            txtIdusuarioTit: '',
            txtIdempresa: '',
            txtNombreusuarioTit: '',
            txtToken: '',

            txtIdproducto: '',
            txtNombreProducto: '',
            txtFechaCompra: '',
            txtIdproveedor: '',
            txtFechaVcto: '',
            txtTipoProducto: '',
            txtPrecioRef: '',
            txtNroFactura: '',
            txtunidadMedida: '',
            valcodigo: 'N'
        }
    }

    componentDidMount = () => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({ txtIdempresa: empresa });
        this.setState({ txtToken: acceso });
        this.setState({ txtIdusuarioTit: usuario });
        this.setState({ txtNombreusuarioTit: nombre });
        this.lecturaInicial(empresa, acceso, usuario);
    }

    lecturaInicial = async (empre, tok, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/qproductos'
        };

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/qproductos?Z=' + tokenDatos, headers)
        this.setState({ product: res.data })
    }

    onChangeNewUser = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));

        return tokenRet;
    }

    numberFormat(num) {
        return (Intl.NumberFormat("de-DE").format(num));
    }

    render() {
        return (
            <Router>
                <div className="navbar navbar-dark bg-dark text-light">
                    <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Consulta de Productos
                           <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p>
                    </h6>
                </div>

                <div className="table-responsive-sm">
                    <table className="table table-striped table-sm">
                        <BootstrapTable
                            keyField='idproducto'
                            data={this.state.product}
                            columns={columns}
                            filter={filterFactory()}
                            filterPosition="top"
                            pagination={paginationFactory(options)}
                        />
                    </table>
                </div>
            </Router>
        )
    }
}