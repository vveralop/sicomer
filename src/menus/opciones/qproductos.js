import React, { Component } from 'react';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class qproductos extends Component {
  constructor(props) {
    super()

    this.state = {
      product: [],
      txtIdusuarioTit: '',
      txtIdempresa: '',
      txtNombreusuarioTit: '',
      txtToken: '',

      txtIdproducto: '',
      txtNombreProducto: '',
      txtFechaCompra: '',
      txtIdproveedor: '',
      txtFechaVcto: '',
      txtTipoProducto: '',
      txtPrecioRef: '',
      txtNroFactura: '',
      txtunidadMedida: '',
      valcodigo: 'N'
    }
  }

  componentDidMount = () => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;

    this.setState({ txtIdempresa: empresa });
    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.lecturaInicial(empresa, acceso, usuario);
  }

  lecturaInicial = async (empre, tok, user) => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/qproductos'
    };

    const tokenDatos = this.codificaToken({
      idempresa: empre,
      idusuario: user,
      token: tok
    });

    const res = await axios.get(urlLogin + '/menu/qproductos?Z=' + tokenDatos, headers)
    this.setState({ product: res.data })

    console.log(res.data)
  }

  productosList = (e) => {
    return (this.state.product.map((prod, index) => (
      <tr key={prod.idproducto}>
        <td className="active small" maxLength="10">{prod.idproducto}</td>
        <td className="active small" maxLength="50">{prod.descripcion}</td>
        <td className="active small" maxLength="50">{prod.nombreproducto}</td>
        <td className="active small text-center" maxLength="10">{prod.fechacompra}</td>
        <td className="active small" maxLength="50">{prod.nombreproveedor}</td>
        <td className="active small text-center" maxLength="10">{prod.fechavencimiento}</td>
        <td className="active small text-right" maxLength="10">{this.numberFormat(prod.precioref)}</td>
        <td className="active small" maxLength="10">{prod.unidadmedida}</td>
      </tr>)))
  }

  onChangeNewUser = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  numberFormat(num) {
    return (Intl.NumberFormat("de-DE").format(num));
  }

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Consulta de Productos
            <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
          </div>
          <div className="table-responsive-sm">
            <table className="table table-striped table-sm">
              <thead className="thead-dark table-sm">
                <tr>
                  <th scope="col" className="small font-weight-bold">Id</th>
                  <th scope="col" className="small font-weight-bold">Tipo Producto</th>
                  <th scope="col" className="small font-weight-bold">Nombre Producto</th>
                  <th scope="col" className="small font-weight-bold text-center">Fecha Compra</th>
                  <th scope="col" className="small font-weight-bold">Proveedor</th>
                  <th scope="col" className="small font-weight-bold text-center">Fecha Vcto</th>
                  <th scope="col" className="small font-weight-bold text-right">Precio</th>
                  <th scope="col" className="small font-weight-bold">Medida Venta</th>
                </tr>
              </thead>
              <tbody className="grid">
                {this.productosList()}
              </tbody>
            </table>
          </div>
        </div>
      </Router>
    )
  }
}