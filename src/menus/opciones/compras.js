import React, { Component } from 'react'
import axios from 'axios';
import jwt from 'jsonwebtoken';
import base64url from 'base64url';
import { data, calculo, urlLogin, iva } from '../../configuraciones/data';
import moment from 'moment';
const secret = base64url.encode(Buffer.from(data), 'utf8');

const detalle = []

export class compras extends Component {
    constructor(props) {
        super()

        this.state = {
            prod: [],
            tpag: [],
            opes: [],
            txtIdusuario: '',
            txtNombreusuario: '',
            txtNombreEmpTit: '',
            txtToken: '',

            txtcodigo: '',
            txtnombre: '',
            txtcantidad: 0,
            txtprecio: 0,
            txttotal: 0,
            txtfechacompra: '',
            txtfpago: 'EFECTIVO',
            txtfactura: 0,
            txtvaloriva: 0,
            txtvalorneto: 0
        }
        // this.onChangelineaD = this.onChangelineaD.bind(this);
        // this.onChangelineaC = this.onChangelineaC.bind(this);
        this.onselectProducto = this.onselectProducto.bind(this);
        this.componentDidMount = this.componentDidMount.bind(this);
        this.traeDatos = this.traeDatos.bind(this);
    }

    componentDidMount = () => {
        this.iniciaIngreso();
        document.getElementById('idproducto').focus();
        document.getElementById('fpago').value = 'EFECTIVO';
    }

    iniciaIngreso() {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;

        this.setState({
            txtToken: acceso,
            txtIdusuario: usuario,
            txtNombreusuario: nombre,
            txtNombreEmpTit: empresa
        });

        //limpia arreglo
        detalle.splice(0, detalle.length);

        document.getElementById('tneto').setAttribute('readOnly', false);
        document.getElementById('tiva').setAttribute('readOnly', false);
        document.getElementById('totalf').setAttribute('readOnly', false);
        document.getElementById('tneto').value = 0
        document.getElementById('tiva').value = 0
        document.getElementById('tfactura').value = 0
        document.getElementById('totalf').value = 0

        document.getElementById('fcompra').value = moment().format("DD-MM-YYYY");
        this.onselectProducto(empresa, acceso, usuario);
        this.traerOperaciones(empresa, acceso, 0, moment().format("DD-MM-YYYY"), usuario);
    }

    onselectProducto = async (empre, tok, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/compras'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            token: tok
        });

        console.log(tokenDatos);

        const res = await axios.get(urlLogin + '/menu/compras?Z=' + tokenDatos, headers)
        this.setState({ prod: res.data })

    }

    traerOperaciones = async (empre, tok, opecpa, fcpa, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/compras/operaciones'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            idcompra: opecpa,
            fechacpa: fcpa,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/compras/operaciones?Z=' + tokenDatos, headers)
        this.setState({ opes: res.data })
    }

    onselectTpago = async () => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/compras/tpago'
        }

        const tokenDatos = this.codificaToken({
            idempresa: this.state.txtNombreEmpTit,
            idusuario: this.state.txtIdusuario,
            token: this.state.txtToken
        });

        const res = await axios.get(urlLogin + '/menu/compras/tpago?Z=' + tokenDatos, headers)
        this.setState({ tpag: res.data })
    }

    prodList() {
        if (this.state.prod !== null) {
            return (
                this.state.prod.map((prod) => (
                    <option className="small" key={prod.idproducto} value={prod.idproducto}>
                        [Código: {prod.idproducto} ] {prod.nombreproducto}
                    </option>)
                )
            )
        }
    }

    tpagoList() {
        if (this.state.tpag === null) {
            return (null)
        } else {
            return (
                this.state.tpag.map((tpag) => (
                    <option className="small" key={tpag.idtipopago} value={tpag.idtipopago}>
                        {tpag.idtipopago} - {tpag.definicion}
                    </option>)
                )
            )
        }
    }

    enterFunction = (event) => {
        if (event.keyCode === '13') {
            alert('Presiono enter');
        }
    }

    onChangelineaC = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    limpiaArreglo = () => {
        document.getElementById('idproducto').value = '';
        document.getElementById('nombreprod').value = '';
        document.getElementById('cantidad').value = 0;
        document.getElementById('precio').value = 0;
        document.getElementById('total').value = 0;

        this.setState((state) => ({
            txtcodigo: '',
            txtnombre: '',
            txtcantidad: 0,
            txtprecio: 0,
            txttotal: 0,
            txtfpago: 'EFECTIVO'
        }));
    }

    agregarLinea = () => {
        if (document.getElementById('idproducto').value === '' ||
            document.getElementById('nombreprod').value === '' ||
            parseFloat(document.getElementById('cantidad').value) <= 0 ||
            parseFloat(document.getElementById('precio').value) <= 0 ||
            parseFloat(document.getElementById('total').value) <= 0) {
            alert('Debe ingresar todos los datos del producto comprado (No se permiten valores negativos en cantidad o Precio).');
        } else {
            const file = {
                codigo: document.getElementById('idproducto').value,
                nombre: document.getElementById('nombreprod').value,
                cantidad: Number(document.getElementById('cantidad').value),
                precio: Number(document.getElementById('precio').value),
                ivalinea: this.redondear((document.getElementById('total').value * iva), 0),
                total: this.redondear(Number(document.getElementById('total').value), 0)
            }
            detalle.push(file);
            this.limpiaArreglo();
    //        document.getElementById('idproducto').focus();
            this.calculaResumen(detalle, 0);
        }
    }

    eliminaLinea = (e) => {
        const registro = e.target.value;
        if (registro !== -1) {
            detalle.splice(registro, 1);
            this.limpiaArreglo();
            document.getElementById('idproducto').focus();
            this.calculaResumen(detalle, 0);
        }
    }

    calculaResumen(arregloDetalle, dsctof) {
        var capital = 0;
        for (var i = 0; i < arregloDetalle.length; i++) {
            capital += parseInt(arregloDetalle[i]['total']);
        }
        capital = capital - dsctof;
        const tiva = this.redondear(capital * iva, 0);
        const tneto = this.redondear(capital * (1 - iva), 0);

        document.getElementById('totalf').value = this.numberFormat(capital);
        document.getElementById('tiva').value = this.numberFormat(tiva);
        document.getElementById('tneto').value = this.numberFormat(tneto);
    }

    itemEncabezadoLista() {
        return (
            <tr>
                <th htmlFor="nom" className="col small">Nombre</th>
                <th htmlFor="cod" className="col small">Código</th>
                <th htmlFor="can" className="col small">Cantidad</th>
                <th htmlFor="pre" className="col small">Precio</th>
                <th htmlFor="tot" className="col small">Total</th>
                <th htmlFor="bor" className="col small">Borrar</th>
            </tr>
        )
    }

    itemLista() {
        if (detalle === null) {
            return (null)
        } else {
            return (
                detalle.map((lis, index) => (
                    <tr key={index}>
                        <td id="nom" className="col small">{lis.nombre}</td>
                        <td id="cod" className="col small">{lis.codigo}</td>
                        <td id="can" className="col small text-right">{this.numberFormat(lis.cantidad)}</td>
                        <td id="pre" className="col small text-right">{this.numberFormat(lis.precio)}</td>
                        <td id="tot" className="col small text-right">{this.numberFormat(lis.total)}</td>
                        <td id="bor" className="col small">
                            <button type="submit"
                                name="botonModal"
                                className="small btn btn-danger text-small"
                                value={index}
                                data-whatever="vm" onClick={this.eliminaLinea}></button>
                        </td>
                    </tr>)))
        }
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));
        return tokenRet;
    }

    traeDatos = (e) => {
        if (this.state.prod !== null) {
            const arreglo = this.state.prod;
            if (this.revisaSiexisteProd(arreglo, e.target.value)) {
                arreglo.map(function (produc) {
                    if (produc.idproducto === e.target.value) {
                        document.getElementById('idproducto').value = produc.idproducto;
                        document.getElementById('nombreprod').value = produc.nombreproducto;
                        document.getElementById('cantidad').value = 1;
                        document.getElementById('precio').value = produc.precioref;
                        document.getElementById('total').value =
                            document.getElementById('precio').value *
                            document.getElementById('cantidad').value;
                        document.getElementById('cantidad').focus();
                        document.getElementById('unidadm').value = produc.unidadmedida;
                        return true;
                    }
                    return null;
                })
            } else {
                document.getElementById('idproducto').focus();
                return false;
            }
        }
    }

    revisaSiexisteProd = (arregloProd, prd) => {
        var ret = false;
        arregloProd.map(function (produc) {
            if (produc.idproducto === prd) {
                ret = true;
            }
            return ret;
        })
        return ret;
    }

    grabaOperacion = async () => {
        if (document.getElementById('totalf').value === '0' ||
            document.getElementById('tfactura').value === '0' ||
            document.getElementById('tneto').value === '0') {
            alert('Faltan datos para grabar operación.')
        } else {
            const encabezado = JSON.stringify({
                idempresa: this.state.txtNombreEmpTit,
                fechacompra: document.getElementById('fcompra').value,
                totalcompra: Number(document.getElementById('totalf').value.replace(/\./g, "")),
                formapagocpa: document.getElementById('fpago').value,
                nrofactura: document.getElementById('tfactura').value,
                ivacompra: Number(document.getElementById('tiva').value.replace(/\./g, "")),
                totalnetocpa: Number(document.getElementById('tneto').value.replace(/\./g, "")),
                lineas: detalle
            });

            const headers = {
                'Content-Type': 'application/json',
                'Access-Control-Allow-Origin': 'http://localhost:5000/menu/compras'
            }

            const tokenDatos = this.codificaToken({
                idempresa: this.state.txtNombreEmpTit,
                idusuario: this.state.txtIdusuario,
                token: this.state.txtToken,
                datos: encabezado
            });

            const res = await axios.post(urlLogin + '/menu/compras', { body: tokenDatos }, headers)
            if (res.status === 200) {
                alert('Operacion generada con folio: ' + res.data['seq']);
                this.iniciaIngreso();
                document.getElementById('idproducto').focus();
            } else {
                alert('Ha habido un error. Avise al Administrador: ' + res.data['registro']);
            }
        }
    }

    eliminaRegistro = () => {
        this.iniciaIngreso();
        document.getElementById('idproducto').focus();
    }

    recalcula() {
        if (document.getElementById('unidadm').value === 'Kg') {
            document.getElementById('cantidad').value = document.getElementById('cantidad').value.replace(/,/g, '.');
        } else {
            document.getElementById('cantidad').value = document.getElementById('cantidad').value.replace(/,/g, '.');
            document.getElementById('cantidad').value = Math.floor(document.getElementById('cantidad').value);
        }
        document.getElementById('precio').value = document.getElementById('precio').value.replace(/,/g, '.');
        const valor = document.getElementById('cantidad').value *
            document.getElementById('precio').value;

        document.getElementById('total').value = valor;
    }

    redondear(num, dec) {
        const p = 10 ** dec;
        const fpFix = Number.EPSILON * (num < 0 ? -1 : 1);
        return Math.round((num + fpFix) * p) / p;
    }

    numberFormat(num) {
        return (Intl.NumberFormat("de-DE").format(num));
    }

    render() {
        return (
            <div>
                <div className="container-fluid text-center bg-dark text-light">Compras Productos-Stock</div>
                <div className="container-fluid text-center small bg-dark text-success">(Usuario: {this.state.txtNombreusuario})</div>
                <div className="container-fluid row px-0">
                    <div className="row text-light bg-dark text-center col-md-5 px-0">
                        <div className="container text-light bg-dark col-md-10 border">Ingresar Productos Compra</div>
                        <div className="container pt-3">
                            <div className="row pl-5 fs-6">
                                <label className="row small bg-dark col-md-3 pl-3" htmlFor="idproducto">Código Producto</label>
                                <input className="small col-md-6" name="txtcodigo" type="text" id="idproducto" placeholder="Código" maxLength="40" onChange={this.onChangelineaC} onBlur={this.traeDatos}/>
                                <select name="txtcodigo" id="selIdpro"
                                    className="small form-select col-md-3"
                                    onChange={this.traeDatos}
                                    value={this.state.txtcodigo}>
                                    <option>Buscar</option>
                                    {this.prodList()}
                                </select>
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="nameprod">Producto</label>
                                <input className="small col-md-9" name="txtnombre" type="text" id="nombreprod" placeholder="Nombre producto" maxLength="50" onChange={this.traeDatos} />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-right pl-3" htmlFor="cantidad">Cantidad</label>
                                <input className="small col-md-3 text-left" name="txtcantidad" type="text" id="cantidad" maxLength="10" placeholder="Cantidad" onBlur={this.recalcula} />
                                <input className="invisible" type="text" id="unidadm" maxLength="10" />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="precio">Precio</label>
                                <input className="small col-md-3" name="txtprecio" type="text" id="precio" maxLength="10" placeholder="Precio" onChange={this.onChangelineaD} onBlur={this.recalcula} />
                            </div>
                            <div className="row pl-5">
                                <label className="row small bg-dark col-md-3 align-middle pl-3" htmlFor="precio">Total</label>
                                <input className="small col-md-3" name="txttotalope" type="text" id="total" maxLength="10" placeholder="Total" readOnly />
                            </div>
                            <div className="container pt-1">
                                <button className="btn btn-success btn-sm border mx-auto" onClick={this.agregarLinea}>Agregar Producto</button>
                            </div>
                            <div className="px-6 pt-2">
                                <div className="col container-fluid text-light bg-dark col-md-10 pb-auto border">Total Compra</div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tneto">Total Neto</label>
                                    <input className="small col-md-3 text-right" name="txtvalorneto" maxLength="40" id="tneto" type="text" onChange={this.onChangelineaD} />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tiva">Iva</label>
                                    <input className="small col-md-3 text-right" name="txtvaloriva" maxLength="40" id="tiva" type="text" onChange={this.onChangelineaD} />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="total">Total de Esta Compra</label>
                                    <input className="small col-md-3 text-right" name="txttotal" maxLength="40" id="totalf" type="text" onChange={this.onChangelineaD} />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="fcompra">Fecha Compra</label>
                                    <input className="small col-md-3 text-right" name="txtfechacompra" maxLength="40" id="fcompra" type="text" onChange={this.onChangelineaC} />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="tfactura">Factura</label>
                                    <input className="small col-md-3 text-right" name="txtfactura" maxLength="40" id="tfactura" type="text" onChange={this.onChangelineaD} required />
                                </div>
                                <div>
                                    <label className="small text-left text-light col-md-4 bg-dark" htmlFor="fpago">Forma de Pago</label>
                                    <select name="txtfpago" id="fpago"
                                        className="small col-md-3 text-left"
                                        onChange={this.onChangelineaC}
                                        onClick={this.onselectTpago}>
                                        {this.tpagoList()}
                                    </select>
                                </div>
                            </div>
                            <button className="btn btn-success btn-sm border" onClick={this.eliminaRegistro}>Eliminar</button>
                            <button className="btn btn-success btn-sm border" onClick={this.grabaOperacion}>Grabar Compra</button>
                        </div>
                    </div>
                    <div className="container-xxl col col-xl-5 pl-3 px-0">
                        <div className="border text-light bg-dark text-center">Detalle de Compra</div>
                        <div className="table table-sm table-striped">
                            <table className="table-responsive table-striped text-left">
                                <thead className="bg-dark text-light">
                                    {this.itemEncabezadoLista()}
                                </thead>
                                <tbody>
                                    {this.itemLista()}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    {/* <div className="col col-xl-2 px-0 bg-dark">
                        <div className="text-light bg-dark text-center">Op Recientes</div>
                        <div className="table table-sm table-striped">
                            <div className="col small align-items-start text-success">
                                operaciones
                            </div>
                        </div>
                    </div> */}
                </div>
            </div>
        )
    }
}

export default compras
