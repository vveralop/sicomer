import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';
import { imprimirElemento }  from '../../controladores/impresion';
import moment from 'moment';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export class opgestion extends Component {
    constructor(props) {
        super()
        this.state = {
            opes: [],
            details: [],
            txtIdusuarioTit: '',
            txtNombreEmpTit: '',
            txtNombreusuarioTit: '',
            txtToken: '',

            txtidempre: '',
            txtnrope: '',
            txttipop: '',
            txtfecha: '',
            txtfpago: '',
            txtusuar: '',
            txttotal: '',
            txtestad: '',
            txtmoned: '',
            txtfechacrea: new Date(),
            txtNombreEmpresa: ''
        }
        this.componentDidMount = this.componentDidMount.bind(this);
        this.imprimeOpe = this.imprimeOpe.bind(this);
        this.traerOperaciones = this.traerOperaciones.bind(this);
    }

    componentDidMount = (e) => {
        const { acceso } = this.props.location.state;
        const { usuario } = this.props.location.state;
        const { nombre } = this.props.location.state;
        const { empresa } = this.props.location.state;
        const { nombreempresa } =  this.props.location.state;

        this.setState({ txtToken: acceso ,
                        txtIdusuarioTit: usuario,
                        txtNombreusuarioTit: nombre,
                        txtNombreEmpTit: empresa,
                        txtNombreEmpresa: nombreempresa });

                        console.log(this.state)
        this.traerOperaciones(empresa, acceso, "", usuario);
        document.getElementById("btnBorrar").className = "btn btn-outline-success btn-sm my-2 my-sm-0";
    }

    onClickModal = (e) => {
        const registro = e.target.value;
        const estadoop = this.state.opes[registro].estado;
        document.getElementById("btnBorrar").className = "btn btn-outline-success btn-sm my-2 my-sm-0";

        //const idempresa = this.state.opes[registro].idempresa;
        const tipo = this.state.opes[registro].tipotx;
        const nroope = this.state.opes[registro].idtx;
        const total = this.state.opes[registro].totaltx;
        const formapag = this.state.opes[registro].fpago;
        const usercrea = this.state.opes[registro].usuariocrea;
        const fechatx = this.state.opes[registro].fechatx;
        const mda = this.state.opes[registro].monedatx;
        const fcrea = this.state.opes[registro].fechacrea;

        this.setState({
            txttipop: tipo,
            txtnrope: nroope,
            txtfecha: fechatx,
            txttotal: mda + '  ' + this.numberFormat(total),
            txtfpago: formapag,
            txtusuar: usercrea,
            txtestad: estadoop,
            txtmoned: mda,
            txtfechacrea: fcrea
        })

        if (estadoop === 'Eliminada') {
            document.getElementById("btnBorrar").className = "btn btn-outline-success btn-sm my-2 my-sm-0 invisible";
        }

        this.traerOperacionesDetalle(this.state.txtNombreEmpTit, this.state.txtToken, 
                                     nroope, tipo, this.state.txtIdusuarioTit);
    }

    cierraModal = (e) => {
        this.limpiaDatos();
    }

    escFunction = (e) => {
        if (e.keyCode === 27) {
            this.limpiaDatos();
        }
    }

    limpiaDatos = () => {
        this.setState({
            txttipoope: '',
            txtidopera: '',
            txtfechaop: '',
            txttotalop: '',
            txtfpago: '',
            txtusercrea: '',
            txtestado: '',
            txtmoned: '',
            txtfechacrea: '',
            txtNombreEmpresa: ''
        })
    }

    onChangeOpe = (e) => {
        this.setState({ [e.target.name]: e.target.value });
    }

    traerOperaciones = async (empre, tok, fcpa, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/opgestion'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user,
            fecha: fcpa,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/opgestion?Z=' + tokenDatos, headers)
        this.setState({ opes: res.data })
    }

    traerOperacionesDetalle = async (empre, tok, nroope, tipope, user) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/opgestion/detalle'
        }

        const tokenDatos = this.codificaToken({
            idempresa: empre,
            idusuario: user, 
            nroope: nroope,
            tipope: tipope,
            token: tok
        });

        const res = await axios.get(urlLogin + '/menu/opgestion/detalle?Z=' + tokenDatos, headers)
        this.setState({ details: res.data })
    }

    colorcondicion1 = (estado) => {
        if (estado === 'Eliminada') {
          return ("active small bold text-danger");
        } else {
          return ("active small");
        }
    }

    detalleOpe() {
        if (this.state.details !== null)
            return (
                this.state.details.map((lis, index) => (
                    <tr key={index}>
                        <td id="nl" className="col-md-1" maxLength="3">{lis.nrolinea}</td>
                        <td id="ip" className="col-md-3" maxLength="40">{lis.idproducto}</td>
                        <td id="np" className="col-md-3" maxLength="50">{lis.nombreproducto}</td>
                        <td id="cn" className="col-md-2 text-right" maxLength="11">{this.numberFormat(lis.cantidad)}  {lis.unidadmedida}</td>
                        <td id="pr" className="col-md-2 text-right" maxLength="11">{this.numberFormat(lis.precio)}</td>
                        <td id="to" className="col-md-2 text-right" maxLength="11">{this.numberFormat(lis.total)}</td>
                    </tr>)))
    }

    colorcondicion1 = (estado) => {
        if (estado === 'Eliminada') {
          return ("small bold text-danger");
        } else {
          return ("small");
        }
    }

    colorcondicion2 = (estado) => {
        if (estado === 'Eliminada') {
          return ("small text-end bold text-danger");
        } else {
          return ("small text-end");
        }
    }

    colorcondicion3 = (estado) => {
        if (estado === 'Eliminada') {
          return ("small text-center bold text-danger");
        } else {
          return ("small text-center");
        }
    }


    productosList = (e) => {
        if (this.state.opes !== null) {
            return (this.state.opes.map((ope, index) => (
                <tr key={index}>
                    <td className={this.colorcondicion1(ope.estado)} maxLength="10">{ope.tipotx}</td>
                    <td className={this.colorcondicion1(ope.estado)} maxLength="10">{ope.fechatx}</td>
                    <td className={this.colorcondicion2(ope.estado)} maxLength="10">{this.numberFormat(ope.idtx)}</td>
                    <td className={this.colorcondicion3(ope.estado)} maxLength="15">{this.numberFormat(ope.totaltx)}</td>
                    <td className={this.colorcondicion1(ope.estado)} maxLength="15">{ope.fpago}</td>
                    <td className={this.colorcondicion1(ope.estado)} maxLength="15">{ope.estado}</td>
                    <td className={this.colorcondicion1(ope.estado)} maxLength="15">{ope.usuariocrea}</td>
                    <td>
                        <button type="submit"
                            name="botonModal"
                            className="btn btn-danger"
                            data-toggle="modal"
                            data-target="#exampleModal"
                            value={index}
                            data-whatever="vm" onClick={this.onClickModal}></button>
                    </td>
                </tr>)))
        } else {
            return (<tr><td className="small" maxLength="10">Sin datos para mostrar</td></tr>)
        }
    }

    anularOpe = async (empre, tok, nroope, tipope) => {
        const headers = {
            'Content-Type': 'application/json',
            'Access-Control-Allow-Origin': 'http://localhost:5000/menu/opgestion'
        }

        const tokenDatos = this.codificaToken({
            idusuario: this.state.txtIdusuarioTit,
            idempresa: this.state.txtNombreEmpTit,
            nroope: this.state.txtnrope,
            tipope: this.state.txttipop,
            token: this.state.txtToken
        });

        const res = await axios.delete(urlLogin + '/menu/opgestion?Z=' + tokenDatos, headers)
        this.traerOperaciones(this.state.txtNombreEmpTit, this.state.txtToken, "", this.state.txtIdusuarioTit);
        console.log(res.data);
        alert(res.data['registro']);
    }

    codificaToken(contenedor) {
        const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
            algorithm: calculo
        }));
        return tokenRet;
    }

    numberFormat(num) {
        return (Intl.NumberFormat("de-DE").format(num));
    }

    imprimeOpe () {

        const fecha = moment(this.state.txtfechacrea).format("DD-MM-YYYY");
        const hora = moment(this.state.txtfechacrea).format("HH:mm:ss");
        const tokenDatos = ({
            fecha: fecha,
            hora: hora,
            tipo: this.state.txttipop.toUpperCase(),
            totalo: this.state.txttotal,
            lineas: this.state.details
        });

        imprimirElemento(this.state.txtNombreEmpresa, tokenDatos, this.state.txtnrope, 2);
    }

    render() {
        return (
            <Router>
                <div>
                    <div className="navbar navbar-dark bg-dark text-light">
                        <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Gestión de Operaciones
                         <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
                    </div>
                    <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" onKeyDown={this.escFunction}>
                        <div className="modal-dialog modal-lg" role="document">
                            <div className="modal-content">
                                <div className="modal-header bg-dark text-white small">
                                    <div className="col">
                                        <label className="bold" id="nrope">Operación: </label>
                                        <input value={this.state.txtnrope}
                                            name="txtnrope" maxLength="15" id="nrope"
                                            type="text" className="medium bg-dark text-white text-center" readOnly />
                                    </div>
                                    <button type="button" className="close" data-dismiss="modal" aria-label="Close" onClick={this.cierraModal}>
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div className="container">
                                    <form className="form-group">
                                        <div className="container">
                                            <div className="input-group input-group-sm mb-2 border small">
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="tipop">Tipo Operación</label>
                                                <input value={this.state.txttipop} name="txttipop" maxLength="15" id="tipop" type="text" className="col-md-3" onChange={this.onChangeOpe}/>
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="fpago">Tipo Pago</label>
                                                <input value={this.state.txtfpago} name="txtfpago" maxLength="15" id="fpago" type="text" className="col-md-3 "  onChange={this.onChangeOpe}/>
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="fecha">Fecha</label>
                                                <input value={this.state.txtfecha} name="txtfecha" maxLength="15" id="fecha" type="text" className="col-md-3"  onChange={this.onChangeOpe}/>
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="total">Total</label>
                                                <input value={this.state.txttotal} name="txttotal" maxLength="15" id="total" type="text" className="col-md-3 text-right" onChange={this.onChangeOpe} />
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="estad">Estado</label>
                                                <input value={this.state.txtestad} name="txtestad" maxLength="15" id="estad" type="text" className="col-md-3"  onChange={this.onChangeOpe}/>
                                                <label className="input-group-text bg-dark text-light col-sm-3" id="inputGroup-sizing-sm" htmlFor="usuar">Usuario Crea</label>
                                                <input value={this.state.txtusuar} name="txtusuar" maxLength="15" id="usuar" type="text" className="col-md-3" onChange={this.onChangeOpe} />
                                            </div>
                                            <div className="input-group input-group mb-2 border">
                                                <div className="table table-striped table border small">
                                                    <table>
                                                    <thead className="thead-dark table">
                                                        <tr>
                                                            <th scope="col" className="col-md-1" htmlFor="nl">N°</th>
                                                            <th scope="col" className="col-md-3" htmlFor="ip">Código Producto</th>
                                                            <th scope="col" className="col-md-3" htmlFor="np">Detalle</th>
                                                            <th scope="col" className="col-md-2 text-right" htmlFor="cn">Cantidad</th>
                                                            <th scope="col" className="col-md-2 text-right" htmlFor="pr">Precio</th>
                                                            <th scope="col" className="col-md-2 text-right" htmlFor="to">Total</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody className="tbody-ligth table">
                                                        {this.detalleOpe()}
                                                    </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                                <div className="modal-footer bg-dark">
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.cierraModal}>Close</button>
                                    <button type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.imprimeOpe}>Imprimir</button>
                                    <button id="btnBorrar" type="button" className="btn btn-outline-success btn-sm my-2 my-sm-0" data-dismiss="modal" onClick={this.anularOpe}>Anular</button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="table-responsive-sm">
                        <table className="table table-striped table-sm">
                            <thead className="thead-dark table-sm">
                                <tr>
                                    <th scope="col" className="small font-weight-bold">Tipo Ope</th>
                                    <th scope="col" className="small font-weight-bold">Fecha</th>
                                    <th scope="col" className="small font-weight-bold">Nro</th>
                                    <th scope="col" className="small font-weight-bold text-center">Total</th>
                                    <th scope="col" className="small font-weight-bold">Forma Pago</th>
                                    <th scope="col" className="small font-weight-bold">Estado</th>
                                    <th scope="col" className="small font-weight-bold">Usuario Crea</th>
                                    <th scope="col" className="small font-weight-bold">Detalle</th>
                                </tr>
                            </thead>
                            <tbody className="grid">
                                {this.productosList()}
                            </tbody>
                        </table>
                    </div>
                </div>
            </Router>
        )
    }
}

export default opgestion;
