import React, { Component } from 'react';
import 'bootstrap/dist/js/bootstrap.bundle';
import 'bootstrap/dist/css/bootstrap.min.css';
import jwt from 'jsonwebtoken';
import axios from 'axios';
import base64url from 'base64url';
import { BrowserRouter as Router } from 'react-router-dom';
import { data, calculo, urlLogin } from '../../configuraciones/data';

const secret = base64url.encode(Buffer.from(data), 'utf8');

export default class dashboard extends Component {
  constructor(props) {
    super()

    this.state = {
      oper: [],
      operprod: [],
      txtIdusuarioTit: '',
      txtIdempresa: '',
      txtNombreusuarioTit: '',
      txtToken: '',

      txtfechasistema: '',
      txtcajainicio: '',
      txttipocalculo: 'Hoy',
      txtfechaflujo: ''

    }

    this.componentDidMount = this.componentDidMount.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount = () => {
    const { acceso } = this.props.location.state;
    const { usuario } = this.props.location.state;
    const { nombre } = this.props.location.state;
    const { empresa } = this.props.location.state;
    this.setState({ txtIdempresa: empresa });
    this.setState({ txtToken: acceso });
    this.setState({ txtIdusuarioTit: usuario });
    this.setState({ txtNombreusuarioTit: nombre });
    this.extraeOperaciones();
  }

  extraeOperaciones = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/dashboard'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtIdempresa,
      idusuario: this.state.txtIdusuarioTit,
      fechaflujo: this.state.txtfechaflujo,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/dashboard?Z=' + tokenDatos, headers);
    if (res.data !== null) {
      this.setState({ oper: res.data })
    }
    console.log(res.data);
    this.calculaResumen(this.state.oper, 'Venta');
    this.calculaResumen(this.state.oper, 'Compra');
  }

  extraeopeProd = async () => {
    const headers = {
      'Content-Type': 'application/json',
      'Access-Control-Allow-Origin': 'http://localhost:5000/menu/dashboard/prod'
    };

    const tokenDatos = this.codificaToken({
      idempresa: this.state.txtIdempresa,
      idusuario: this.state.txtIdusuarioTit,
      fechaflujo: this.state.txtfechaflujo,
      token: this.state.txtToken
    });

    const res = await axios.get(urlLogin + '/menu/dashboard/prod?Z=' + tokenDatos, headers);
    if (res.data !== null) {
      this.setState({ operprod: res.data })
    }

    // this.calculaResumenProd(this.state.operprod, 'Venta');
    // this.calculaResumenProd(this.state.operprod, 'Compra');
  }

  handleChange = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  }

  onChangeTipoCalculo(valor) {
    const fecha = new Date();
    if (valor === 'Hoy') {
      this.setState({ txtfechaflujo: fecha.getDate() })
    }
  }

  codificaToken(contenedor) {
    const tokenRet = JSON.stringify(jwt.sign(contenedor, secret, {
      algorithm: calculo
    }));

    return tokenRet;
  }

  numberFormat(num) {
    return (Intl.NumberFormat("de-DE").format(num));
  }

  calculaResumen(arregloDetalle, tipo) {
    var capital = 0;
    var iva = 0;
    var total = 0;
    if (arregloDetalle !== null) {
      for (var i = 0; i < arregloDetalle.length; i++) {
        if (arregloDetalle[i]['tipotx'] === tipo) {
          capital += parseInt(arregloDetalle[i]['totaltx']);
          iva += parseInt(arregloDetalle[i]['ivatx']);
        }
      }
      total = capital - iva;
      return (
        <tr>
          <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(capital)} </td>
          <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(iva)} </td>
          <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(total)} </td>
        </tr>
      )
    }
  }

  calculaResumenProd(arregloDetalle, tipo) {
    var cantidad = 0;
    var iva = 0;
    var total = 0;
    var diff = 0;
    if (arregloDetalle !== null) {
      for (var i = 0; i < arregloDetalle.length; i++) {
        if (arregloDetalle[i]['tipotx'] === tipo) {
          cantidad = parseInt(arregloDetalle[i]['cantidad']);
          iva = parseInt(arregloDetalle[i]['iva']);
          total = parseInt(arregloDetalle[i]['total']);
          diff = total - iva;
          return (
            <tr>
              <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(cantidad)} </td>
              <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(total)} </td>
              <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(iva)} </td>
              <td className="active col-sm-12 text-right" maxLength="15">{this.numberFormat(diff)} </td>
            </tr>
          )
        }
      }
    }
  }

  // hacer resumen por tipo de pago

  render() {
    return (
      <Router>
        <div>
          <div className="navbar navbar-dark bg-dark text-light">
            <h6 className="ms-auto my-2 my-sm-0 mx-auto text-center">Cuadro de Mando
            <p className="text-center text-success small">(Usuario: {this.state.txtNombreusuarioTit})</p></h6>
          </div>
          <div className="container-fluid">
            <div className="row text-light bg-dark text-center col-md-12 px-0">
              <label className="row small mx-2 bg-dark text-light text-center" htmlFor="cajaini">Ingrese Caja Inicial</label>
              <input className="small col-md-1" id="cajaini" type="text" name="txtcajainicio" value={this.state.txtcajainicio} onChange={this.handleChange}/>
              <button className="btn btn-success btn-sm border" onClick={this.grabaOperacion}>Calcular</button>
            </div>


            <div className="container row mx-0 my-1 ms-2 col-sm-10 border rounded">
              <div className="container row mx-1 my-1 ms-2 col-sm-4 border rounded">
                  {this.calculaResumen(this.state.oper, 'Venta')}
                  {this.calculaResumen(this.state.oper, 'Compra')}
              </div>
              <div className="container row mx-1 my-1 ms-2 col-sm-4 border rounded">
                hola2
              </div>
              <div className="container row mx-1 my-1 ms-2 col-sm-3 border rounded">
                hola3
              </div>
              <div className="container row mx-1 my-1 ms-2 col-sm-3 border rounded">
                hola4
              </div>
            </div>
          </div>
        </div>
      </Router>
    )
  }
}