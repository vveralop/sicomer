import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import { BrowserRouter as Router, Route, Link } from 'react-router-dom';
import Users from './opciones/users';
import Empresas from './opciones/empresas';
import Secuencias from './opciones/secuencias';
import Productos from './opciones/Productos';
import Tipoproductos from './opciones/tipoproducto';
import Modalcamclave from './opciones/modalcamclave';
import Proveedores from './opciones/proveedores';
import Compras from './opciones/compras';
import Ventas from './opciones/ventasbeta';
import Opgestion from './opciones/opgestion';
import Qstock from './opciones/qstock';
//import Qproductos from './opciones/qproductos';
import Dashboard from './opciones/dashboard';
import Qproductos from './opciones/prodquery';
import Inicio from './opciones/iniciadia';
import Fin from './opciones/findia';
import Genbarcode from './opciones/codigobarra';
import App from '../App';

export class Sicomer extends Component {
    constructor() {
        super()

        this.state = {
            txtUsuario: "",
            txtNombre: "",
            accesoSistema: "",
            txtEmpresa: "",
            txtnombreEmpresa: '',
            logMenu: false,
            esAdmin: false
        }

        this.componentDidMount = this.componentDidMount.bind(this);
    }

    componentDidMount() {
        const acceso = this.props.acceso;
        const usuario = this.props.usuario;
        const nombre = this.props.nombre;
        const empresa = this.props.empresa;
        const nombreempresa = this.props.nombreempresa;

        if (empresa === '77249123-9') {
            this.setState({ esAdmin: true });
        }

        this.setState({
            txtEmpresa: empresa,
            accesoSistema: acceso,
            txtUsuario: usuario,
            txtNombre: nombre,
            txtnombreEmpresa: nombreempresa
        });
    }

    componentWillUnmount() {
        this.setState({ logMenu: false })
    }

    render() {
        var esAdmin = this.state.esAdmin;
        return (
            <Router>
                <div>
                    {esAdmin ?
                        <div className="navbar navbar-expand-lg navbar-dark bg-dark text-light">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <h3 className="navbar-brand">{this.state.txtnombreEmpresa}</h3>
                            <ul className="nav nav-pills mx-auto">
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Operaciones</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/compras",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Compras</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/ventas",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Ventas</Link>
                                        <div className="dropdown-divider"></div>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/opgestion",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreEmpresa
                                                }
                                            }}>Gestión de Operaciones</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/dashboard",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Cuadro de Mando</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Gestión</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/qstock",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Consulta de Stock</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/qproductos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Consulta de Productos</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Parametría</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/tproductos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Tipos de Productos</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/proveedores",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Proveedores</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/productos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Productos</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/genbarcode",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Generación Códigos de Barra</Link>

                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Usuarios</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/cambioclave",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario
                                                }
                                            }}>Cambio de Clave</Link>
                                        {/* <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/inicio",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Inicio Día</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/fin",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Fin Día</Link> */}
                                    </div>
                                </li>
                                <li className="nav-item dropdown disabled">
                                    <a id="menuadm" className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Clientes</a>
                                    {this.inhabilita}
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/empresas",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Mantención Empresas</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/users",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Mantiene Usuarios</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/secuencias",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Mantiene Secuencias</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/dashboard",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Cuadro de Mando</Link>
                                    </div>
                                </li>
                            </ul>
                            <ul className="navbar-nav ml-auto">
                                <Link id="salir" className="nav-link mr-auto text-light" to="/menu">Salir</Link>
                            </ul>
                        </div>
                        :
                        <div className="navbar navbar-expand-lg navbar-dark bg-dark text-light">
                            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span className="navbar-toggler-icon"></span>
                            </button>

                            <h3 className="navbar-brand">{this.state.txtnombreEmpresa}</h3>
                            <ul id="menuSistema" className="nav nav-pills mx-auto visible">
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Operaciones</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/compras",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Compras</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/ventas",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Ventas</Link>
                                        <div className="dropdown-divider"></div>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/opgestion",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Gestión de Operaciones</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/dashboard",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa,
                                                    nombreempresa: this.state.txtnombreempresa
                                                }
                                            }}>Cuadro de Mando</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Gestión</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/qstock",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Consulta de Stock</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/qproductos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Consulta de Productos</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Parametría</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/tproductos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Tipos de Productos</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/proveedores",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Proveedores</Link>

                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/productos",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Mantiene Productos</Link>
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/genbarcode",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario,
                                                    nombre: this.state.txtNombre,
                                                    empresa: this.state.txtEmpresa
                                                }
                                            }}>Generación Códigos de Barra</Link>
                                    </div>
                                </li>
                                <li className="nav-item dropdown">
                                    <a className="nav-link dropdown-toggle text-success bg-dark border-success" data-toggle="dropdown" href="/#" role="button" aria-haspopup="true" aria-expanded="false">Usuarios</a>
                                    <div className="dropdown-menu">
                                        <Link className="dropdown-item text-success"
                                            to={{
                                                pathname: "/menu/cambioclave",
                                                state: {
                                                    acceso: this.state.accesoSistema,
                                                    usuario: this.state.txtUsuario
                                                }
                                            }}>Cambio de Clave</Link>
                                    </div>
                                </li>
                            </ul>
                            <ul className="navbar-nav ml-auto">
                                <Link id="salir" className="nav-link mr-auto text-light" to="/menu">Salir</Link>
                            </ul>
                        </div>
                    }
                    <Route exact path="/menu/users" component={Users} />
                    <Route exact path="/menu/empresas" component={Empresas} />
                    <Route exact path="/menu/secuencias" component={Secuencias} />
                    <Route exact path="/menu/productos" component={Productos} />
                    <Route exact path="/menu/tproductos" component={Tipoproductos} />
                    <Route exact path="/menu/proveedores" component={Proveedores} />
                    <Route exact path="/menu/cambioclave" component={Modalcamclave} />
                    <Route exact path="/menu/compras" component={Compras} />
                    <Route exact path="/menu/ventas" component={Ventas} />
                    <Route exact path="/menu/opgestion" component={Opgestion} />
                    <Route exact path="/menu/qstock" component={Qstock} />
                    <Route exact path="/menu/qproductos" component={Qproductos} />
                    <Route exact path="/menu/dashboard" component={Dashboard} />
                    <Route exact path="/menu/inicio" component={Inicio} />
                    <Route exact path="/menu/fin" component={Fin} />
                    <Route exact path="/menu" component={App} />
                    <Route exact path="/menu/genbarcode" component={Genbarcode} />
                </div>
            </Router>
        )
    }
}

export default Sicomer;
